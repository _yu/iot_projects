#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>

/**
 * @author Elena Rughi, Yuqi Sun
 * */

/* pins */
#define LED_GREEN 4 /* D2 <=> GPIO4 */
#define LED_RED 5   /* D1 <=> GPIO5 */
#define POT A0

/* utils */
#define WMAX 1000 /* dumpster weight capacity */
enum states{
  AVAILABLE,
  UNAVAILABLE
} state;

/* wifi network name */
char* ssidName = "eMotoG5";
//char* ssidName = "_yuHopeWorld";
/* WPA2 PSK password */
char* pwd = "978de76f3869";
//char * pwd = "qs6z771mmj5jg";
/* service IP address */ 
char* address = "http://707cbf8f.ngrok.io";

int currWeight = -1;
bool forced = false;

void setup() { 
  Serial.begin(115200);  
  pinMode(LED_GREEN, OUTPUT);
  pinMode(LED_RED, OUTPUT);              
  state = AVAILABLE;
  
  WiFi.begin(ssidName, pwd);
  Serial.print("Connecting...");
  while (WiFi.status() != WL_CONNECTED) {  
    delay(500);
    Serial.print(".");
  } 
  Serial.println("Connected: \n local IP: "+WiFi.localIP());
  
}

int sendData(String address, String component, int weight, bool isAvailable){  
  HTTPClient http;    
  http.begin(address + "/api/data");      
  http.addHeader("Content-Type", "application/json");     
  String msg = 
  "{ \"component\": \"" + String(component) + 
  "\", \"weight\": " + String(weight) +  
  ", \"state\": " + String(isAvailable) + " }";
  int retCode = http.POST(msg);   

  if(retCode == HTTP_CODE_OK){
    const String& payload = http.getString();
    Serial.print("payload: ");
    Serial.println(payload);

    if(payload.indexOf("unavailable") > 0){
      state = UNAVAILABLE;
      forced = true;
    } else if(payload.indexOf("available") > 0){
      state = AVAILABLE;
      weight = 0;
      forced = true;
    }
  }

  http.end();  
    
  //String payload = http.getString();  
  //Serial.println(payload);      
  return retCode;
}
   
void loop() { 

  if (WiFi.status()== WL_CONNECTED){   
    int weight = 0;
    /* if status was not forced, read pot */
    if(!forced){
      /* check waste weight */
      weight = analogRead(POT);
      Serial.println("Current Weight: " + String(weight));
      
      if(weight < WMAX ){
        state = AVAILABLE;
      } else {
        state = UNAVAILABLE;
      }
    }

    /* send msg to server only if weight changed */
    if(weight < currWeight-1 || weight > currWeight+1){
      currWeight = weight;
      
      /* toggle green-red leds accordingly */
      switch(state){
        case AVAILABLE: 
          digitalWrite(LED_GREEN, HIGH);
          digitalWrite(LED_RED, LOW);
          break;
        case UNAVAILABLE:
          digitalWrite(LED_GREEN, LOW);
          digitalWrite(LED_RED, HIGH);
          break;
      }
      forced = false;
      
      /* send data */
      Serial.print("sending w:"+String(weight)+" a:"+String(state==AVAILABLE)+" ...");    
      int code = sendData(address, "edge", weight, state==AVAILABLE);
      
      /* log result */
      if (code == 200){
        Serial.println("ok");   
      } else {
        Serial.println("error");
      }
      delay(3000); /* to avoid overwhelming the server*/
    }
     
  } else { 
    Serial.println("Error in WiFi connection");   
  }
     
}
