package dataService;

import clientsManagement.*;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Elena Rughi, Yuqi Sun
 * */

/*
 * Data Service as a vertx event-loop 
 */
public class DataService extends AbstractVerticle {

	private int port;
	private Edge edge;
	private MobileApp app;
	private Dashboard dashboard;
	private Map<String, Client> clients;
	private Dumpster dumpster;
	
	public DataService(int port) {
		this.port = port;
		this.dumpster = new Dumpster();
		this.edge = new Edge(dumpster);
		this.app = new MobileApp(dumpster);
		this.dashboard = new Dashboard(dumpster);
		this.clients = new HashMap<>();
		clients.put("edge", edge);
		clients.put("dashboard", dashboard);
		clients.put("app", app);
	}

	@Override
	public void start() {		
		/* "A router receives request from an HttpServer and routes it to the first matching Route that it contains" */
		Router router = Router.router(vertx);
		
		Set<String> allowedHeaders = new HashSet<>();
	    allowedHeaders.add("x-requested-with");
	    allowedHeaders.add("Access-Control-Allow-Origin");
	    allowedHeaders.add("origin");
	    allowedHeaders.add("Content-Type");
	    allowedHeaders.add("accept");
	    allowedHeaders.add("X-PINGARUNER");

	  Set<HttpMethod> allowedMethods = new HashSet<>();
	    allowedMethods.add(HttpMethod.GET);
	    allowedMethods.add(HttpMethod.POST);
	    allowedMethods.add(HttpMethod.OPTIONS);

	  router.route().handler(CorsHandler.create("*").allowedHeaders(allowedHeaders).allowedMethods(allowedMethods));
		router.route().handler(BodyHandler.create()); /* add route and append a request handler to it */
		router.post("/api/data").handler(this::handlePostData);
		router.get("/api/data").handler(this::handleGetData);	
		vertx
			.createHttpServer()
			.requestHandler(router::accept)
			.listen(port);

		log("Service ready.");
	}

	
	/* Routing Context: "a new instance is created for each HTTP request that is received 
	 * it provides access to HttpServerRequest and HttpServerResponse and allows to
	 * maintain data for the lifetime of the context;
	 * contexts are discarded once they have been routed to the handler for the request.
	 * The context also provides access to the Session"  */
	
	private void handlePostData(RoutingContext routingContext) {
		HttpServerResponse response = routingContext.response();
		response.putHeader("Content-Type", "application/json");
		log("new msg "+routingContext.getBodyAsString());
		
		/* JsonObject: HTTP request body in json format
		 * it contains values set by the client, corresponding
		 * to keys also set by the client */
		JsonObject res = routingContext.getBodyAsJson();
		if (res == null) {
			sendError(400, response);
		} else {
			/* pass data on to the right client, as specified in msg */
			Client currClient = clients.get(res.getString("component"));
			String logMsg = currClient.handleData(res);			
			System.out.println(logMsg);
			response.setStatusCode(200).end(currClient.getReturnMsg());
		}
	}
	
	private void handleGetData(RoutingContext routingContext) {
		/* UNUSED */
	}
	
	private void sendError(int statusCode, HttpServerResponse response) {
		response.setStatusCode(statusCode).end();
	}

	private void log(String msg) {
		System.out.println("[DATA SERVICE] "+msg);
	}

	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();
		DataService service = new DataService(8080);
		vertx.deployVerticle(service, new DeploymentOptions().setWorker(true));
	}
}