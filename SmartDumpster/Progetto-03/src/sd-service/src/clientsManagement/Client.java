package clientsManagement;

import io.vertx.core.json.JsonObject;

/**
 * @author Elena Rughi, Yuqi Sun
 * */

public interface Client {
	/* chose to pass JsonObject as argument instead of Map<String, Object>
	 * because JsonObject supports directly primitive types 
	 * instead of dealing with the Object casting */
	/** @return log message to be displayed */
	public String handleData(JsonObject data);
	
	public String getReturnMsg();
}
