package clientsManagement;

import io.vertx.core.json.JsonObject; 

public class Edge implements Client{
	private Dumpster dumpster;

	public Edge(Dumpster dumpster) {
		this.dumpster = dumpster;
	}
	
	/** @param: data contains [int weight, bool isAvailable] */
	@Override
	public String handleData(JsonObject data) {
		this.dumpster.setWeight(data.getInteger("weight"));
		if(data.getInteger("state")==1) {
			this.dumpster.setState("available");
		} else {
			this.dumpster.setState("unavailable");
		}
		return "[EDGE] current weight: " + this.dumpster.getWeight() + ", dumpster status: " + this.dumpster.isAvailable();
	}

	@Override
	public String getReturnMsg() {
		if(this.dumpster.getForceState() != "") {
			JsonObject json = new JsonObject();
			/* pass on eventual forced state change */
			json.put("forceState", this.dumpster.getForceState());
			this.dumpster.setForceState("");	//reset
			return json.encodePrettily();
		} else {
			return "";
		}
	}
}
