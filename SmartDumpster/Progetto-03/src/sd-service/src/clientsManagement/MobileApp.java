package clientsManagement;

import java.time.LocalDate;
import java.util.List;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;

import io.vertx.core.json.JsonObject;

public class MobileApp implements Client {
	private Dumpster dumpster;
	private int prevWeight;
	private String token;
	
	public MobileApp(Dumpster dumpster) {
		this.dumpster = dumpster;
		this.prevWeight = this.dumpster.getWeight();
		this.token = "notAvailable";
	}
	
	/* to be called when dumpster is emptied */
	public void reset() {
		this.dumpster.empty();
		this.prevWeight = 0;
	}
	
	/* this function registers a deposit's date and weight */
	private void depositSuccessful() {
		this.dumpster.incNumDeposits();
		LocalDate currDate = LocalDate.now();
		//System.out.println(currDate.toString());
		/* if there isn't already an entry for current date, make one*/
		if(!this.dumpster.getDepositsLog().isDateLogged(currDate)) {
			this.dumpster.getDepositsLog().createLog(currDate);
		}
		/* if currWeight > actual weight then the dumpster must have been
		 * emptied, meaning previous weight should be 0 */
		if(this.dumpster.getWeight() < this.prevWeight) {
			this.prevWeight = 0;
		}
		/* add to current date the recently added weight
		 * then update weight */
		this.dumpster.getDepositsLog().logDeposit(currDate, this.dumpster.getWeight() - this.prevWeight);
		this.prevWeight = this.dumpster.getWeight();
	}
	
	@Override
	public String handleData(JsonObject data) {
		String log = "[MOBILE APP] msg";
		/* token request */
		if(data.getBoolean("tokenRequest") != null && data.getBoolean("tokenRequest")) {
			this.token = this.dumpster.getToken();
			log = log + "; token requested";
		}
		/* deposit successful */
		if(data.getBoolean("depositSuccessful") != null && data.getBoolean("depositSuccessful")) {
			this.depositSuccessful();
			log = log + "; deposit was successful";
		} else if(data.getBoolean("depositSuccessful") != null && !data.getBoolean("depositSuccessful")){
			log = log + "; deposit was unsuccessful";
		}
		return log;
	}

	@Override
	public String getReturnMsg() {
		JsonObject json = new JsonObject();
		json.put("token", this.token);
		return json.encodePrettily();
	}

}
