package clientsManagement;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class Dashboard implements Client{
	private Dumpster dumpster;
	private int days;
	
	public Dashboard(Dumpster dumpster) {
		this.dumpster = dumpster;
		this.days = 0;
	}
	
	@Override
	public String getReturnMsg() {
		JsonObject json = new JsonObject();
		json.put("msg", "hello");
		if(this.dumpster.isAvailable()) {
			json.put("state", "available");
		} else {
			json.put("state", "not available");
		}
		json.put("weight", this.dumpster.getWeight());
		json.put("nDeposits", this.dumpster.getNumDeposits());
		/* get deposit info from the last n registered days */
		json.put("datesSelected", new JsonArray(this.dumpster.getDepositsLog().getInfo(this.days)));
		return json.encodePrettily();
	}

	@Override
	public String handleData(JsonObject data) {
		String log = "[DASHBOARD] Request received";
		/* if present, get days input to display deposit data */
		try {
			if(data.getInteger("days") >= 0) {
				this.days = data.getInteger("days");
				log = log + "; sending data from last " + this.days + " days";
			}
		} catch (ClassCastException e) {
			if(Integer.parseInt(data.getString("days")) >= 0) {
				this.days = Integer.parseInt(data.getString("days"));
				log = log + "; sending data from last " + this.days + " days";
			}
		}
		
		/* if present, get forceState input */
		if(data.getString("forceState") != "") {
			this.dumpster.setForceState(data.getString("forceState"));
			log = log + "; force dumpster state to " + this.dumpster.getForceState();
		}
		return log;
	}
}
