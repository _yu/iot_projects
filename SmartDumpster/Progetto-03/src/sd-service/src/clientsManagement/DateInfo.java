package clientsManagement;

/**
 * @author Elena Rughi, Yuqi Sun
 * */

public class DateInfo {
	private String date;
	private int nDeposits;
	private int weight;
	
	public DateInfo(String date, int nDeposits, int weight) {
		this.date = date;
		this.nDeposits = nDeposits;
		this.weight = weight;
	}
	
	public String getDate() {
		return this.date;
	}
	
	public int getNumDeposits() {
		return this.nDeposits;
	}
	
	public int getWeight() {
		return this.weight;
	}

}
