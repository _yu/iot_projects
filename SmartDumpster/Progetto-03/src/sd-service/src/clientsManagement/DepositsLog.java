package clientsManagement;

import java.util.Collections;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Elena Rughi, Yuqi Sun
 * */

public class DepositsLog {
	private Map<LocalDate, List<Integer>> depositsByDate = new TreeMap<LocalDate, List<Integer>>();
	
	public DepositsLog() {
		/* for testing */
		depositsByDate.put(LocalDate.now(), new LinkedList<Integer>());
		Collections.addAll(depositsByDate.get(LocalDate.now()), 3, 45);
		
		depositsByDate.put(LocalDate.parse("2019-12-14"), new LinkedList<Integer>());
		Collections.addAll(depositsByDate.get(LocalDate.parse("2019-12-14")), 2, 4, 1, 1);

		depositsByDate.put(LocalDate.parse("2019-12-16"), new LinkedList<Integer>());
		Collections.addAll(depositsByDate.get(LocalDate.parse("2019-12-16")), 23, 5, 23, 65, 87, 9);
		
		depositsByDate.put(LocalDate.parse("2019-12-17"), new LinkedList<Integer>());
		Collections.addAll(depositsByDate.get(LocalDate.parse("2019-12-17")), 45, 23, 7);
		
		depositsByDate.put(LocalDate.parse("2019-12-28"), new LinkedList<Integer>());
		Collections.addAll(depositsByDate.get(LocalDate.parse("2019-12-28")), 22, 34, 66, 1, 23, 5, 7, 8, 2, 5);
	}
	
	public List<String> getDates(int n){
		List<String> list = new LinkedList<>();
		if(depositsByDate.keySet().size() < n) {
			n = depositsByDate.keySet().size();
		}
		depositsByDate.keySet().stream().sorted().skip(depositsByDate.keySet().size()-n)
			.forEach((date) -> {
				list.add(date.toString());
			});
		return list;
	}
	
	/**
	 * Get deposit information for the past n days
	 * @param n the number of days of which info is requested
	 * @return a map {date, {nDepositsPerDay, weightPerDay}}
	 */
	public List<DateInfo> getInfo(int n){
		List<DateInfo> list = new LinkedList<>();
		if(depositsByDate.keySet().size() < n) {
			n = depositsByDate.keySet().size();
		}
		getDates(n).forEach((date) -> {
			list.add(new DateInfo(date.toString(), this.getNumDepositsByDate(date.toString()), this.getWeightByDate(date.toString())));
		});
		return list;
	}
	
	public boolean isDateLogged(LocalDate date) {
		return depositsByDate.containsKey(date);
	}
	
	public void createLog(LocalDate date) {
		depositsByDate.put(date, new LinkedList<Integer>());
	}
	
	public void logDeposit(LocalDate date, int weight) {
		depositsByDate.get(date).add(weight);
	}
	
	/* currently unused */
	public int getTotNumDeposits() {
		List<Integer> list = new LinkedList<>();
		this.getDates(9999).forEach((date) -> {
			list.add(this.getNumDepositsByDate(date));
		});
		return list.stream().reduce(0, Integer::sum);
	}
	
	/** 
	 * @param date format: "YYYY-MM-DD" 
	 * @return 0 if there are no entries for given date 
	 */
	public int getNumDepositsByDate(String date) {
		LocalDate targetDate = LocalDate.parse(date);
		if(depositsByDate.containsKey(targetDate)) {
			return depositsByDate.get(targetDate).size();
		} else {
			return 0;
		}
	}
	
	/** 
	 * Returns the total weight deposited in given date
	 * @param date format: "YYYY-MM-DD" 
	 * @return 0 if there are no entries for given date 
	 */
	public int getWeightByDate(String date) {
		LocalDate targetDate = LocalDate.parse(date);
		if(depositsByDate.containsKey(targetDate)) {
			return depositsByDate.get(targetDate).stream().mapToInt(Integer::intValue).sum();
		} else {
			return 0;
		}
	}
}
