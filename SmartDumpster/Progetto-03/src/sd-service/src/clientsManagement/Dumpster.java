package clientsManagement;

import java.security.SecureRandom;
import java.time.LocalDate;
import java.util.Base64;

/**
 * @author Elena Rughi, Yuqi Sun
 * */

public class Dumpster {
	private int weight;
	private enum Status{
		AVAILABLE, UNAVAILABLE
	}
	Status state;
	private String forceState;
	private int nDeposits;
	private DepositsLog depositsLog;
	
	public Dumpster() {
		this.state = Status.AVAILABLE;
		this.forceState = "";
		this.depositsLog = new DepositsLog();
		this.nDeposits = this.depositsLog.getNumDepositsByDate(LocalDate.now().toString());
		this.weight = this.depositsLog.getWeightByDate(LocalDate.now().toString());
	}
	
	public DepositsLog getDepositsLog() {
		return this.depositsLog;
	}
	
	public boolean isAvailable() {
		return this.state == Status.AVAILABLE;
	}
	
	public void setState(String state) {
		if(state == "available") {
			this.state = Status.AVAILABLE;
		} else {
			this.state = Status.UNAVAILABLE;
		}
	}
	
	public int getWeight() {
		return this.weight;
	}
	
	public void setWeight(int weight) {
		if(weight>=0) {
			this.weight = weight;
		} else {
			System.out.println("[ERROR]Invalid weight value");
		}
	}
	
	public String getForceState() {
		return this.forceState;
	}
	
	public void setForceState(String setState) {
		this.forceState = setState;
	}
	
	public int getNumDeposits() {
		return this.nDeposits;
	}
	
	public void incNumDeposits() {
		this.nDeposits++;
	}
	
	public void empty() {
		this.nDeposits = 0;
	}
	
	public String getToken() {
		if(this.isAvailable()) {
			SecureRandom random = new SecureRandom();
			byte bytes[] = new byte[6];
			random.nextBytes(bytes);
			Base64.Encoder base64Encoder = Base64.getUrlEncoder();
			return base64Encoder.encodeToString(bytes);
		} else {
			return "notAvailable";
		}
	}

}
