$(document).ready(function(){
  /* authors Elena Rughi, Yuqi Sun */

  var address = "http://707cbf8f.ngrok.io";
  $("#addrLabel").text(address);

  $("button#setAddress").click(function(){
    address = $("#inputAddress").val();
    $("#addrLabel").text(address);
  });

  /* request info from last n days */
  var nDays = 0;
  $("button#setDays").click(function(){
    let input = $("#inputDays").val();
    if($.isNumeric(input) && input >=0 ){
      nDays = input;
    } else {
      nDays = 0;
    }
    $("#labelDays").text(nDays);
  });

  /* force Dumpster State */
  var forceState = "";
  $("button#forceAvailable").click(function(){
    forceState = "available";
  });

  $("button#forceUnavailable").click(function(){
    forceState = "unavailable";
  })

  /* --------------- POST ----------------*/
  var doNextFetch = false;
  var timeInterval = 5 * 1000;
	$("label#connectionStatus").text("paused");
  var fetchInfo = function() {
    console.log('Sending AJAX request...');
    $.ajax({
      type: "POST",
      url: address + "/api/data",
      data: JSON.stringify(
        {
          "component" : "dashboard",
          "days": nDays,
          "forceState": forceState
        }
      ),
      contentType: "application/json",
      dataType: "json",
      success: function(json){
        console.log("success");
        displayData(json);
      },
      error: function(jqXHR, textStatus, errorThrown){
        /* pause request fetching */
        doNextFetch = false;
        $("label#connectionStatus").text("paused");
        
        displayError();
        
        console.log("jqXHR: " + jqXHR.status);
        console.log("textStatus: " + textStatus);
        console.log("errorThrown: " + errorThrown);
      },
      complete: function() {
        // Schedule the next request after this one completes,
        // even after error
        console.log('Waiting ' + (timeInterval / 1000) + ' seconds');
        if(doNextFetch){
          setTimeout(fetchInfo, timeInterval);
        }
        forceState = ""; //reset
      }
    });
  };

  /* start data retrieval */
  $("button#start").click(function(){
	if(!doNextFetch){
    doNextFetch = true;
    $("label#connectionStatus").text("yes");
      fetchInfo();
    }
  });
  
  /* stop data retrieval */
  $("button#stop").click(function(){
	doNextFetch = false;
	$("label#connectionStatus").text("paused");
  });
});

function displayData(json){
  $("#whole-msg").text(JSON.stringify(json));
  $("#state-span").text(JSON.parse(JSON.stringify(json.state))); 
  $("#weight-span").text(JSON.parse(JSON.stringify(json.weight)));  
  $("#deposits-span").text(JSON.parse(JSON.stringify(json.nDeposits)));
  fillTable(json);
}

function fillTable(json){
  $("tr").not(":first").empty();
  $.each(json.datesSelected, function(index, value){
    let date = json.datesSelected[index].date;
    let nDeposits = json.datesSelected[index].numDeposits;
    let weight = json.datesSelected[index].weight;
    $("table tr:last").after("<tr><td headers=\"day\">" + date + "</td><td headers=\"nDeposits\">" + nDeposits + "</td><td headers=\"weight\">" + weight + "</td></tr>");
  })
}

function displayError(){
  $("span").text("---");
  $("#whole-msg").text("error; see console for details");
}