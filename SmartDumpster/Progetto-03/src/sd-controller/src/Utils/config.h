/**
 * @author Elena Rughi, Yuqi Sun
 * */

#ifndef __CONFIG__
#define __CONFIG__

/* time(milliseconds) */
#define TIME_DELIVER 5000
#define TIME_ADD 4000

/* distance(cm) */
#define PIN_LED_A 13
#define PIN_LED_B 12
#define PIN_LED_C 11
#define PIN_SERVO 6

#endif
