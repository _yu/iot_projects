#include <Arduino.h>
#include "src/Hardware/Led.h"
#include "src/Utils/config.h"
#include "src/MsgService/MsgService.h"
#include "src/Utils/Servo/ServoMotorImpl.h"

/**
 * @author Elena Rughi, Yuqi Sun
 * */

#define T_DELIVER 10000 /* available time to deposit trash */
#define T_ADD 5000
unsigned long startTime = 0;
bool timerRunning = false;
int delta = 0;

MsgServiceBT msgService(2,3);
Led* ledA = new Led(PIN_LED_A);
Led* ledB = new Led(PIN_LED_B);
Led* ledC = new Led(PIN_LED_C);
ServoMotorImpl* servo = new ServoMotorImpl(PIN_SERVO);

void setup() {
  msgService.init();  
  Serial.begin(9600);
  while (!Serial){}
  servo->on();
  servo->setPosition(0);
  Serial.println("ready to go."); 
}

void loop() {
  if (msgService.isMsgAvailable()) {
    Msg* msg = msgService.receiveMsg();
    Serial.println(msg->getContent());    
    if (msg->getContent() == "A ON") {
      ledA->switchOn();
    } else if (msg->getContent() == "B ON") {
      ledB->switchOn();
    } else if (msg->getContent() == "C ON") {
      ledC->switchOn();
    }  else if (msg->getContent() == "Time") {
      delta += T_ADD;
    }

    /* after a led has turned on, start timer
     * and move servo motor */
    if( msg->getContent().indexOf("ON") > 0 && !timerRunning){
      startTimer();
      servo->setPosition(170);
    }

    /* time has elapsed: send app confirmation; move servo motor */
    delete msg;
  }
  if(timerRunning && timerEnded()){
    msgService.sendMsg("{ \"depositSuccessful\": \"true\" }");
    switchAllOff();
    servo->setPosition(0);
    delta = 0;
  }
}

void startTimer(){
  startTime = millis();
  timerRunning = true;
}

bool timerEnded(){
  unsigned long currTime = millis();
  if (currTime - startTime > T_DELIVER + delta) {
    timerRunning = false;
    return true;
  } else {
    return false;
  }
}

void switchAllOff() {
  ledA->switchOff();
  ledB->switchOff();
  ledC->switchOff();
}
