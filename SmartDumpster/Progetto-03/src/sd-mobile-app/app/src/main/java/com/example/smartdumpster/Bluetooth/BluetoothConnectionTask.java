package com.example.smartdumpster.Bluetooth;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.util.UUID;

/*A class that creates the connection between the local device and the Arduino bluetooth module*/
public final class BluetoothConnectionTask extends ConnectionTask {

    private BluetoothSocket btSocket = null;

    public BluetoothConnectionTask(final BluetoothDevice serverBtDevice, final UUID uuid, final EventListener eventListener) {
        try {
            this.btSocket = serverBtDevice.createRfcommSocketToServiceRecord(uuid);
            super.setEventListener(eventListener);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Integer doInBackground(Void... unused) {
        try {
            this.btSocket.connect();
        } catch (IOException e) {
            e.printStackTrace();
            try {
                this.btSocket.close( );
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return ConnectionTask.getConnectionCanceled();
        }
        super.setConnectedChannel(new RealBluetoothChannel(this.btSocket));
        return ConnectionTask.getConnectionDone();
    }
}

