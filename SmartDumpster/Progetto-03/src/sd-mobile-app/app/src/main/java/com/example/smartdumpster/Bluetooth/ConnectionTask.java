package com.example.smartdumpster.Bluetooth;

import android.os.AsyncTask;

public abstract class ConnectionTask extends AsyncTask<Void, Void, Integer> {

    private static final int CONNECTION_DONE = 1;
    private static final int CONNECTION_CANCELED = 2;

    private BluetoothChannel connectedChannel;
    private EventListener eventListener;

    @Override
    protected void onPostExecute(Integer result) {
        switch (result) {
            case CONNECTION_DONE:
                if (this.eventListener != null) {
                    this.eventListener.onConnectionActive(this.connectedChannel);
                }
                break;
            case CONNECTION_CANCELED:
                if( this.eventListener != null) {
                    this.eventListener.onConnectionCanceled();
                }
                break;
        }
    }

    public void setConnectedChannel(final BluetoothChannel connectedChannel) {
        this.connectedChannel = connectedChannel;
    }

    public void setEventListener(final EventListener eventListener) {
        this.eventListener = eventListener;
    }

    public static int getConnectionCanceled() {
        return CONNECTION_CANCELED;
    }

    public static int getConnectionDone() {
        return CONNECTION_DONE;
    }

    public interface EventListener{
        void onConnectionActive(final BluetoothChannel channel);
        void onConnectionCanceled();
    }
}

