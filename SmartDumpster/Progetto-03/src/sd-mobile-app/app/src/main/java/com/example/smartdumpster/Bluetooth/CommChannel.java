package com.example.smartdumpster.Bluetooth;

/*An interface for the communication between the local device and the connected one*/

public interface CommChannel {

    void close();

    void registerListener(Listener listener);

    void removeListener(Listener listener);

    void sendMessage(String message);

    interface Listener {
        void onMessageReceived(String receivedMessage);
    }
}


