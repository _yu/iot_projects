package com.example.smartdumpster.Handlers;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.TextView;

/*probably useless since using worker threads should be fine for such a small project*/
public class MainUiHandler extends Handler {

    public static final int WASTE_DEPOSIT_DONE_MSG = 1;
    public static final String WASTE_DEPOSIT_DONE_VALUE = "waste-deposit-done";

    public MainUiHandler(final Looper looper, final TextView label){
        super(looper);
    }

    @Override
    public void handleMessage(Message msg) {
        switch(msg.what){
            case WASTE_DEPOSIT_DONE_MSG:
                //int newValue = msg.getData().getInt(NEW_CNT_VALUE);
                break;

            default:
                throw new UnsupportedOperationException();
        }
    }
}

