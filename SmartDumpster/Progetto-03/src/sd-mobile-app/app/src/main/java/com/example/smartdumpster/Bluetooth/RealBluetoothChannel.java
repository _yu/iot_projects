package com.example.smartdumpster.Bluetooth;

import android.bluetooth.BluetoothSocket;
import android.os.Message;
import android.util.Log;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

public final class RealBluetoothChannel extends BluetoothChannel {

    RealBluetoothChannel(BluetoothSocket socket){
        worker = new BluetoothWorker(socket);
        new Thread(worker).start();
    }

    class BluetoothWorker implements ExtendedRunnable {
        private final BluetoothSocket socket;
        private final InputStream inputStream;
        private final OutputStream outputStream;

        BluetoothWorker(BluetoothSocket socket) {
            this.socket = socket;

            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                tmpIn = socket.getInputStream();
            } catch (IOException e) {
                Log.e("CLIENT", "Error occurred when creating input stream", e);
            }
            try {
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e("CLIENT", "Error occurred when creating output stream", e);
            }

            inputStream = tmpIn;
            outputStream = tmpOut;
        }

        public void run() {
            while (socket.isConnected()) {
                try {
                    DataInputStream input = new DataInputStream(inputStream);

                    StringBuffer readbuffer = new StringBuffer();

                    byte inputByte;

                    while ((inputByte = input.readByte()) != 0) {
                        char chr = (char) inputByte;
                        if(chr != MessagesUtils.getMessageTerminator()){
                            readbuffer.append(chr);
                        } else {
                            String inputString = readbuffer.toString();
                            Message receivedMessage = btChannelHandler.obtainMessage(0, inputString.getBytes());
                            receivedMessage.sendToTarget();

                            readbuffer = new StringBuffer();
                        }
                    }
                }
                catch(Exception e){
                    e.printStackTrace();
                }
            }
        }

        public void write(byte[] bytes) {
            try {
                byte[] bytesToBeSent = Arrays.copyOf(bytes, bytes.length+1);
                bytesToBeSent[bytesToBeSent.length -1] = '\n';

                outputStream.write(bytesToBeSent);

                Message writtenMsg = btChannelHandler.obtainMessage(1, bytes);
                writtenMsg.sendToTarget();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void cancel() {
            try {
                socket.close();
            } catch (IOException e) {
                Log.e("CLIENT", "Could not close the connect socket", e);
            }
        }
    }
}

