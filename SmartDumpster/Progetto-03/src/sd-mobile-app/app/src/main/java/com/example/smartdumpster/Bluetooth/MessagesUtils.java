package com.example.smartdumpster.Bluetooth;

public class MessagesUtils {

    private static final String LIB_TAG = "BluetoothLib";
    private static final String ARDUINO_BLUETOOTH_NAME = "isi03";
    private static final String ARDUINO_BLUETOOTH_UIID = "7ba55836-01eb-11e9-8eb2-f2801f1b9fd1";
    private static final char MESSAGE_TERMINATOR = '\n';

    public static String getLibTag() {
        return LIB_TAG;
    }

    public static String getArduinoBluetoothName() {
        return ARDUINO_BLUETOOTH_NAME;
    }

    public static String getArduinoBluetoothUuid() {
        return ARDUINO_BLUETOOTH_UIID;
    }

    public static char getMessageTerminator() {
        return MESSAGE_TERMINATOR;
    }

    public class emulator {
        public static final String HOST_IP = "10.0.2.2";
        public static final int HOST_PORT = 8080;
    }
}
