package com.example.smartdumpster.Main;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Objects;

import android.bluetooth.BluetoothDevice;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.smartdumpster.Bluetooth.*;
import com.example.smartdumpster.R;
import com.example.smartdumpster.Http.*;

import java.util.UUID;

/**
 * @author Elena Rughi, Yuqi Sun
 * */

public class MainActivity extends AppCompatActivity {

    final String url = " http://707cbf8f.ngrok.io/api/data";
    private RadioGroup radioGroup;
    private RadioButton wasteA, wasteB, wasteC;
    private Button sendBtn, addTimeBtn;
    private BluetoothChannel btChannel;
    private String msg;
    private String token = "notAvailable";
    private boolean on = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initUI();
    }

    private void initUI() {
        this.wasteA = findViewById(R.id.wasteA);
        this.wasteB = findViewById(R.id.wasteB);
        this.wasteC = findViewById(R.id.wasteC);
        this.radioGroup = findViewById(R.id.radioGroup);

        this.sendBtn = findViewById(R.id.sendBtn);
        this.sendBtn.setOnClickListener(view1 -> this.sendWaste());

        this.addTimeBtn = findViewById(R.id.addTimeBtn);
        this.addTimeBtn.setOnClickListener(view2 -> this.addMoreTime());

        findViewById(R.id.bluetoothSwitch).setOnClickListener(view3 -> this.connectBluetoothClickEvent());
        findViewById(R.id.serverBtn).setOnClickListener(view4 -> this.httpPost("check"));
        findViewById(R.id.getTokenBtn).setOnClickListener(view5 -> this.httpPost("token"));
        this.enableButtons();
    }

    /*--------------------- functions ---------------------*/

    private void httpPost(String select){
        try {
            switch(select){
                case "check": checkServerClickEvent(); break;
                case "token": tryHttpGetToken(); break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void checkServerClickEvent() throws JSONException {

        final String content = new JSONObject()
                .put("component", "app").toString();
        checkNetworkClickEvent();
        Http.post(url, content.getBytes(), response -> {
            if (response == null) {
                ((TextView)findViewById(R.id.serverStatus)).setText("Waiting for server...");
            } else {
                if (response.code() == HttpURLConnection.HTTP_OK) {
                    ((TextView)findViewById(R.id.serverStatus)).setText("Server is connected");
                } else {
                    ((TextView)findViewById(R.id.serverStatus)).setText("Server error: " + (response.code()));
                }
            }
        });
    }

    private void tryHttpGetToken() throws JSONException {

        final String content = new JSONObject()
                .put("component", "app")
                .put("tokenRequest", true).toString();

        Http.post(url, content.getBytes(), response -> {
            if (response == null) {
                ((TextView)findViewById(R.id.serverStatus)).setText("Waiting for server...");
            } else {
                if (response.code() == HttpURLConnection.HTTP_OK) {
                    try {
                        ((TextView)findViewById(R.id.serverStatus)).setText("Server is connected");
                        final JSONObject json = new JSONObject(response.contentAsString());
                        this.token = json.getString("token");
                        ((TextView) findViewById(R.id.tokenLabel)).setText("Token: " + this.token);
                        this.enableButtons();
                        findViewById(R.id.getTokenBtn).setEnabled(false);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException j) {
                        j.printStackTrace();
                    }
                } else {
                    ((TextView)findViewById(R.id.serverStatus)).setText("Server error: " + (response.code()));
                }
            }
        });
    }



    private void checkNetworkClickEvent() {
        final ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        final NetworkInfo activeNetwork = Objects.requireNonNull(cm).getActiveNetworkInfo();
        if(activeNetwork != null && activeNetwork.isConnectedOrConnecting()){
            ((TextView)findViewById(R.id.network)).setText("Network is available");
        } else {
            ((TextView)findViewById(R.id.network)).setText("Network is not available");
        }
    }

    private void connectBluetoothClickEvent() {
        try {
            if (!this.on) {
                this.connectToArduino();
                this.on = true;
            } else {
                btChannel.close();
                this.on = false;
                ((TextView) findViewById(R.id.bluetoothStatus)).setText(
                        String.format("Status : unable to connect"));
            }
            this.enableButtons();
        } catch (BluetoothDeviceNotFound bluetoothDeviceNotFound) {
            bluetoothDeviceNotFound.printStackTrace();
        }
    }

    private void connectToArduino() throws BluetoothDeviceNotFound {
        final BluetoothDevice serverDevice = BluetoothUtils.getPairedDeviceByName(MessagesUtils.getArduinoBluetoothName());
        final UUID uuid = BluetoothUtils.getEmbeddedDeviceDefaultUuid();
        //final UUID uuid = BluetoothUtils.generateUuidFromString(MessagesUtils.getArduinoBluetoothUuid());
        new BluetoothConnectionTask(serverDevice, uuid, new ConnectionTask.EventListener() {

            @Override
            public void onConnectionActive(final BluetoothChannel channel) {
                ((TextView) findViewById(R.id.bluetoothStatus)).setText(
                        String.format("Status : connected"));
                btChannel = channel;
                btChannel.registerListener(new RealBluetoothChannel.Listener() {
                    @Override
                    public void onMessageReceived(String receivedMessage) {
                        try{
                            final JSONObject json = new JSONObject(receivedMessage);
                            if(json.getBoolean("depositSuccessful")) {
                                final String content = new JSONObject()
                                        .put("component", "app")
                                        .put("depositSuccessful", true).toString();

                                Http.post(url, content.getBytes(), response -> {
                                    if(response == null || response.code() != HttpURLConnection.HTTP_OK ){
                                        ((TextView)findViewById(R.id.serverStatus)).setText("Server error: " + (response.code()));
                                    }
                                });
                                ((TextView) findViewById(R.id.tokenLabel)).setText("");
                                token = "notAvailable";
                                enableButtons();
                                findViewById(R.id.getTokenBtn).setEnabled(true);
                            }
                        } catch(JSONException e){
                            /* depositSuccessful message was not sent */
                        }
                    }
                });
            }

            @Override
            public void onConnectionCanceled() {
                ((TextView) findViewById(R.id.bluetoothStatus)).setText(
                        String.format("Status : unable to connect"));
            }
        }).execute();
    }

    private boolean getWasteType() {
        int selectedId = radioGroup.getCheckedRadioButtonId();
        boolean done = false;
        if (this.wasteA.getId() == selectedId) {
            msg = "A ON";
            done = true;
        } else if (this.wasteB.getId() == selectedId) {
            msg = "B ON";
            done = true;
        } else if (this.wasteC.getId() == selectedId) {
            msg = "C ON";
            done = true;
        }
        return done;
    }

    private void sendWaste() {
        if (this.getWasteType()) {
            this.btChannel.sendMessage(msg);
        }
    }

    private void addMoreTime() {
        this.btChannel.sendMessage("Time");
    }

    private void enableButtons() {
        if (!this.token.equals("notAvailable") && this.on) {
            this.wasteA.setEnabled(true);
            this.wasteB.setEnabled(true);
            this.wasteC.setEnabled(true);
            this.sendBtn.setEnabled(true);
            this.addTimeBtn.setEnabled(true);
        } else {
            this.wasteA.setEnabled(false);
            this.wasteB.setEnabled(false);
            this.wasteC.setEnabled(false);
            this.sendBtn.setEnabled(false);
            this.addTimeBtn.setEnabled(false);
        }
    }
}
