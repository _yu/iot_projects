package com.example.smartdumpster.Bluetooth;

public interface ExtendedRunnable extends Runnable {
    void write(byte[] bytes);
    void cancel();
}
