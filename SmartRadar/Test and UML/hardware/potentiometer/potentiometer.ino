#include "Pot.h"

#define POT_PIN A0

Pot* pot;
int current;

void setup(){
  Serial.begin(9600);
  pot = new Pot(POT_PIN);
}

void loop(){
  int newValue = pot->getValue();
  if (newValue != current){
    current = newValue;
    Serial.println(current);
  }
}
