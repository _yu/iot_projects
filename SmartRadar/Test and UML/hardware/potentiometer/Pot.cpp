#include "Pot.h"
#include "Arduino.h"

Pot::Pot(int pin){
  this->pin = pin;
}

int Pot::getValue(){
  return analogRead(pin);
}