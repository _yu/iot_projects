#ifndef __POT__
#define __POT__

class Pot{
private:
  int pin;
public:
  Pot(int pin);
  int getValue();
};

#endif
