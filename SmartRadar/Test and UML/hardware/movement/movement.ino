#include "ServoMotor.h"

#define MOTOR_PIN 9

int pos;   
int delta;
ServoMotor* motor;

void setup(){
  Serial.begin(9600);
  motor = new ServoMotor(MOTOR_PIN);
  pos = 0;
  delta = 1;
}

void loop(){
  motor->turnOn();
  for (int i = 0; i < 180; i++) {
    Serial.println(pos);
    motor->moveTo(pos);         
    delay(5);            
    pos += delta;
  }
  motor->turnOff();
  pos -= delta;
  delta = -delta;
  delay(1000);
}

