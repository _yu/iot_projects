#ifndef __SERVOMOTOR__
#define __SERVOMOTOR__

#include "Servo.h"

class ServoMotor{
private:
  int pin;
  Servo servo;
public:
  ServoMotor(int pin);
  void turnOn();
  void turnOff();
  void moveTo(int pos);
  int getPos();
};

#endif
