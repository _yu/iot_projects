#include "ServoMotor.h"

ServoMotor::ServoMotor(int pin){
  this->pin = pin;
}

void ServoMotor::turnOn(){
  this->servo.attach(pin);
}

void ServoMotor::turnOff(){
  this->servo.detach();
}

void ServoMotor::moveTo(int dir){
  this->servo.write(dir);
}

int ServoMotor::getPos(){
  return this->servo.read();
}
