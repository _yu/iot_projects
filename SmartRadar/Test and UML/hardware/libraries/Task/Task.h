#ifndef __TASK__
#define __TASK__

class Task {
  int myPeriod;
  int timeElapsed;
  
public:
  virtual void init(int period){
    myPeriod = period;
    timeElapsed = 0;
  }

  virtual void setPeriod(int period){
    myPeriod = period;
  }

  virtual void tick() = 0;

  /* returns true if task's period has elapsed
   * which means the task should be executed*/
  bool updateAndCheckTime(int basePeriod){
    timeElapsed += basePeriod;
    if (timeElapsed >= myPeriod){
      timeElapsed = 0;
      return true;
    } else {
      return false; 
    }
  }
};

#endif

