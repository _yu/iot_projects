#include "Task.h"
#include "BlinkTask.h"
#include "Scheduler.h"

#define LED_PIN 13

Scheduler scheduler;

void setup(){
  scheduler.init(50);
  
  Task* t0 = new BlinkTask(LED_PIN);
  t0->init(500);
  scheduler.addTask(t0);
}

void loop(){
  scheduler.schedule();
};
