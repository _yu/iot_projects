#ifndef __PIR__
#define __PIR__

#define CALIBRATION_TIME_SEC 10

class Pir{
private:
    int pin;
    /* depending on how pir is activated, may have to 
      make public 
      at the moment it's called at the creation of the object
    */
    void calibrate();

public:
    Pir(int pin);
    bool movDetected();
};

#endif
