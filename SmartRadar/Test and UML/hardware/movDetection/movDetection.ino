#include "Pir.h"

#define PIR_PIN 2

Pir* pir;

void setup(){
  Serial.begin(9600);
  pir = new Pir(PIR_PIN);
}

void loop(){
  if(pir->movDetected()){
    Serial.println("movement detected");
  } else {
    Serial.println("...");
  }
  delay(500);
}
