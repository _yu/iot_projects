#ifndef __DETECTTASK__
#define __DETECTTASK__

#include "Task.h"
#include "Sonar.h"

class DetectTask: public Task{
private:
  int trigPin;
  int echoPin;
  Sonar* sonar;

public:
  DetectTask(int trigPin, int echoPin);  
  void init(int period);  
  void tick();
};

#endif
