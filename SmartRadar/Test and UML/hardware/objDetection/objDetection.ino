#include "DetectTask.h"
#include "Scheduler.h"

#define TRIG_PIN 8
#define ECHO_PIN 7

Scheduler scheduler;

void setup(){
  Serial.begin(9600);
  scheduler.init(100);
  Task* t0 = new DetectTask(TRIG_PIN, ECHO_PIN);
  t0->init(1000);
  scheduler.addTask(t0);
}

void loop(){
  scheduler.schedule();
}
