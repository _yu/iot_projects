#include "DetectTask.h"
#include "Arduino.h"

DetectTask::DetectTask(int trigPin, int echoPin){
  this->trigPin = trigPin;
  this->echoPin = echoPin;
}

void DetectTask::init(int period){
  Task::init(period);
  sonar = new Sonar(trigPin, echoPin);
}

void DetectTask::tick(){
  float dist = sonar->getDistance();
  Serial.println(dist);
}