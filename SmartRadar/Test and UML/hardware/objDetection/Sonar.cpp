#include "Sonar.h"
#include "Arduino.h"

Sonar::Sonar(int trigPin, int echoPin){
  this->trigPin = trigPin;
  this->echoPin = echoPin;
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);  
}

float Sonar::getDistance(){
  /* supposing temperature = 20° */
  const double vs = 331.45 + 0.62*20;
  /* send impulse */
  digitalWrite(trigPin,LOW);
  delayMicroseconds(3);
  digitalWrite(trigPin,HIGH);
  delayMicroseconds(5);
  digitalWrite(trigPin,LOW);
    
  /* receive echo */
  long tUS = pulseInLong(echoPin, HIGH);
  
  double t = tUS / 1000.0 / 1000.0 / 2;
  double d = t*vs;
  return d;
}
