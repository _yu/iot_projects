#include "Button.h"

#define BTN1_PIN 8
#define BTN2_PIN 9
#define BTN3_PIN 10

Button* btn1;
Button* btn2;
Button* btn3;

void setup(){
  Serial.begin(9600);
  btn1 = new Button(BTN1_PIN);
  btn2 = new Button(BTN2_PIN);
  btn3 = new Button(BTN3_PIN);
}

void loop(){
  if (btn1->isPressed()){
    Serial.println("1");
  }
  if (btn2->isPressed()){
    Serial.println("3");
  }
  if (btn3->isPressed()){
    Serial.println("2");
  }
}
