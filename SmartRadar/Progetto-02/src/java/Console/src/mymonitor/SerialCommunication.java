package mymonitor;

import java.util.Arrays;
import java.util.List;
import java.util.Observable;
import java.util.stream.Collectors;

import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import jssc.SerialPortList;

/**
 * @author Elena Rughi, Yuqi Sun
 * */

public class SerialCommunication extends Observable implements SerialPortEventListener {

	private SerialPort serialPort;
	private StringBuilder currentMessage;

	public void start(String portName) {
	    this.currentMessage = new StringBuilder();
		serialPort = new SerialPort(portName);
		try {
		    serialPort.openPort();

		    serialPort.setParams(SerialPort.BAUDRATE_9600,
		                         SerialPort.DATABITS_8,
		                         SerialPort.STOPBITS_1,
		                         SerialPort.PARITY_NONE);

		    serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_RTSCTS_IN |
		                                  SerialPort.FLOWCONTROL_RTSCTS_OUT);

		    serialPort.addEventListener(this);
		}
		catch (SerialPortException ex) {
		    System.out.println("There are an error on writing string to port т: " + ex);
		}
	}

	/**
	 * This should be called when you stop using the port.
	 * This will prevent port locking on platforms like Linux.
	 */
	public synchronized void close() {
		try {
			if (serialPort != null) {
				serialPort.removeEventListener();
				serialPort.closePort();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	
	/*------------------------ OTHER METHODS -----------------------------------*/

	
	/* Checks if there are bytes received in the input buffer. It reads String from the serial;
	 * because the Serial reads the received string little by little, we first need to receive the
	 * complete message sent by Serial; to do so it checks if the string given by serialPort.readString
	 * ends with "\n": if it does we notify the view;*/

	@Override
	public synchronized void serialEvent(final SerialPortEvent event) {
        if(event.isRXCHAR() && event.getEventValue() > 0) {
            try {
                final String received = serialPort
                        .readString(event.getEventValue());
                this.currentMessage.append(received);
                if(received.contains("\n")) {
                    this.setChanged();
                    this.notifyObservers();
                }
            } catch (SerialPortException ex) {
                System.out.println("Error in receiving string from COM-port: " + ex);
            }
        }
    }
	
	public void sendMsg(String msg) {
		char[] array = (msg+"\n").toCharArray();
		byte[] bytes = new byte[array.length];
		for (int i = 0; i < array.length; i++){
			bytes[i] = (byte) array[i];
		}
		try {
			synchronized (serialPort) {
				serialPort.writeBytes(bytes);
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public synchronized String receiveMsg() {
        final String completeString = this.currentMessage.toString();
        this.currentMessage = new StringBuilder();
        return completeString;
    }

	public List<String> getPorts() {
	    return Arrays.stream(SerialPortList.getPortNames())
	                 .collect(Collectors.toList());
	}
}
