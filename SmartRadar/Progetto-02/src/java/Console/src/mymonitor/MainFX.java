package mymonitor;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * @author Elena Rughi, Yuqi Sun
 * */

public class MainFX extends Application {
    
    private SerialCommunication serialComm = new SerialCommunication();
    
	public static void main(String[] args) {
		launch();
	}

	public void start(Stage stage) throws Exception {
		new ViewFX(stage, serialComm);
	}
}
