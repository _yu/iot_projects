package mymonitor;

import java.util.Observable;
import java.util.Observer;

import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.ArrayList;
import java.util.Collection;
import java.lang.Integer;

import javafx.scene.Node;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

/**
 * @author Elena Rughi, Yuqi Sun
 * */

public class ViewFX implements Observer{
    
	private static final int ANGLE_MIN = 0;
	private static final int ANGLE_MAX = 180;
	private static final int TIME_MIN = 2;
	private static final int TIME_MAX = 10;
	
	private SerialCommunication serialComm;
	private TextArea textArea = new TextArea();
	private Label insertDir;
    private TextField inputDir;
    private Label insertTime;
    private TextField inputTime;
    private ToggleButton pot;
    private TextArea textAreaSA;
    private TextArea textAreaM;

	public ViewFX(final Stage stage,  final SerialCommunication serialComm) {
		
		this.serialComm = serialComm;
		stage.setTitle("Smart Radar");
		FlowPane root = new FlowPane();
		
		/* ---------------- MODE SELECTION ---------------*/
		FlowPane selectMode = new FlowPane(10, 10);
		Label modeLabel = new Label("Select system mode");
		modeLabel.setDisable(true);
		
		ToggleButton btnManual = new ToggleButton("Manual");
		ToggleButton btnSingle = new ToggleButton("Single");
		ToggleButton btnAuto = new ToggleButton("Auto");
		ToggleGroup tg = new ToggleGroup();
		btnManual.setToggleGroup(tg);
		btnSingle.setToggleGroup(tg);
		btnAuto.setToggleGroup(tg);
		
		Collection<Node> modeBtns = new ArrayList<Node>();
		modeBtns.add(btnManual);
		modeBtns.add(btnSingle);
		modeBtns.add(btnAuto);
		disableNodes(modeBtns);
		
		selectMode.getChildren().addAll(modeLabel, btnManual, btnSingle, btnAuto);	
		
		/* ------------------- DISPLAY --------------------*/
		FlowPane display = new FlowPane(10, 10);
		textArea.setPrefColumnCount(80);
		textArea.setPrefRowCount(20);
		textArea.setEditable(false);
		textArea.setWrapText(true);
		
		ScrollPane scrollPane = new ScrollPane(textArea);
		scrollPane.setFitToHeight(true);
		scrollPane.setFitToWidth(true);
		scrollPane.setFitToHeight(true);
		
		display.getChildren().add(scrollPane);
		
		/* ----------------- PORT COMBOBOX -----------------*/
		FlowPane choosePort = new FlowPane(10, 10);
		Label selectPort = new Label("Select Port");
		ComboBox<String> comboBox = new ComboBox<String>();
		this.serialComm.getPorts().stream().forEach(s -> {
            comboBox.getItems().add(s);
        });
        
		Button openPort = new Button("Open Port");
		openPort.setDisable(true);
		Button closePort = new Button("Close Port");
		closePort.setDisable(true);
		
		choosePort.getChildren().addAll(selectPort, comboBox, openPort, closePort);
		
		
		/*----------------- TEXT FIELD FOR MANUAL MODE ---------------*/
		FlowPane inputManual = new FlowPane(10, 10);
		insertDir = new Label("Insert a direction between " + ANGLE_MIN + " and " + ANGLE_MAX + " degrees");
		insertDir.setDisable(true);
		inputDir = new TextField();
		inputDir.setMaxWidth(100);
		inputDir.setDisable(true);
		
		textAreaM = new TextArea();
		textAreaM.setPrefColumnCount(30);
		textAreaM.setPrefRowCount(4);
		textAreaM.setEditable(false);
		textAreaM.setWrapText(true);
		textAreaM.setDisable(true);
		
		inputManual.getChildren().addAll(insertDir, inputDir, textAreaM);
		
		
		/*----------------- TEXT FIELD FOR SINGLE AND AUTO MODE ---------------*/
		FlowPane inputSingleAuto = new FlowPane(10, 10);
		insertTime = new Label("Insert an integer scan period between " + TIME_MIN + "s and " + TIME_MAX + "s");
		insertTime.setDisable(true);
		inputTime = new TextField();
		inputTime.setMaxWidth(100);
		inputTime.setDisable(true);
		
		textAreaSA = new TextArea();
		textAreaSA.setPrefColumnCount(30);
		textAreaSA.setPrefRowCount(4);
		textAreaSA.setEditable(false);
		textAreaSA.setWrapText(true);
		textAreaSA.setDisable(true);
		
		pot = new ToggleButton("Input Time with Potentiometer");
		pot.setDisable(true);
		
		inputSingleAuto.getChildren().addAll(insertTime, inputTime, pot, textAreaSA);
		
		
		/*------------------- ROOT MANAGEMENT -----------------*/
		root.getChildren().addAll(choosePort, selectMode, display, inputManual, inputSingleAuto);
		for(Node n : root.getChildren()) {
			FlowPane.setMargin(n, new Insets(20));
		}
		stage.setScene(new Scene(root, 1000, 700));
		stage.show();		
		
		
		/* -------------------- ACTIONS ----------------------*/
	
		/* PORT SELECTION */
		comboBox.setOnAction((e) -> {
			if(!comboBox.getSelectionModel().isEmpty()) {
				textArea.appendText("Selected port " + comboBox.getSelectionModel().getSelectedItem() + "\n");
				openPort.setDisable(false);
			}
		});

		/* OPEN CONNECTION */
		openPort.setOnAction((e) -> {
		  this.serialComm.start(comboBox.getSelectionModel().getSelectedItem());
			textArea.appendText("Port open!\n\n");
			enableNodes(modeBtns);
			enableNodes(Stream.of(closePort, inputDir, insertDir, modeLabel, textAreaM)
					.collect(Collectors.toList()));
			disableNodes(Stream.of(openPort, comboBox, selectPort)
					.collect(Collectors.toList()));
		});
		
		/* CLOSE CONNECTION */
		closePort.setOnAction((e) -> {
			this.serialComm.close();
			textArea.appendText("Closing port ... \n\n");
			disableNodes(modeBtns);
			disableNodes(Stream.of(inputDir, inputTime, insertDir, insertTime, pot, modeLabel, openPort, closePort, textAreaM, textAreaSA)
						.collect(Collectors.toList()));
			enableNodes(Stream.of(comboBox, selectPort)
						.collect(Collectors.toList()));
			comboBox.getSelectionModel().clearSelection();
			pot.setSelected(false);
			btnManual.setSelected(true);
		});
		
		/* MODE SELECTION */
		btnManual.setOnAction((e) -> {
			this.serialComm.sendMsg("M");
			enableNodes(Stream.of(inputDir, insertDir, textAreaM)
					.collect(Collectors.toList()));
			disableNodes(Stream.of(inputTime, insertTime, pot, textAreaSA)
					.collect(Collectors.toList()));
			btnManual.setSelected(false);
		});
		
		btnSingle.setOnAction((e) -> {
			this.serialComm.sendMsg("S");
			enableNodes(Stream.of(insertTime, pot, textAreaSA)
					.collect(Collectors.toList()));
			disableNodes(Stream.of(inputDir, insertDir, textAreaM)
					.collect(Collectors.toList()));
			pot.setSelected(false);
			inputTime.setDisable(false);
			btnSingle.setSelected(false);
		});
		
		btnAuto.setOnAction((e) -> {
		    this.serialComm.sendMsg("A");
		    enableNodes(Stream.of(insertTime, pot, textAreaSA)
					.collect(Collectors.toList()));
			disableNodes(Stream.of(inputDir, insertDir, textAreaM)
					.collect(Collectors.toList()));
			pot.setSelected(false);
			inputTime.setDisable(false);
			btnAuto.setSelected(false);
		});
		
		/* INPUT DIRECTION IN MANUAL MODE */
		inputDir.setOnAction((e) -> {
			String msg = inputDir.getText();
			if(checkInputDir(msg)) {
				textAreaM.appendText("Selected direction: " + msg +'\n');
				this.serialComm.sendMsg("P: " + msg);
			} else {
				textAreaM.appendText("Invalid direction input \n");
			}
			inputDir.setText("");
		});
		
		/* INPUT TIME IN SINGLE/AUTO MODE */
		inputTime.setOnAction((e) -> {
			String msg = inputTime.getText();
			if(checkInputTime(msg)) {
				Integer time = Integer.parseInt(msg)*1000;
				this.serialComm.sendMsg("T: " + time.toString());
				textAreaSA.appendText("Selected scan time: " + time/1000 + '\n');
			} else {
				textAreaSA.appendText("Invalid scan time input \n");
			}	
			inputTime.setText("");
		});
		
		/* INPUT TIME WITH POTENTIOMETER */    
		pot.setOnAction((e) -> {
			if(pot.isSelected()) {
			    this.serialComm.sendMsg("Pot");
				inputTime.setDisable(true);
				insertTime.setDisable(true);
			} else {
			    this.serialComm.sendMsg("No pot");
				inputTime.setDisable(false);
				insertTime.setDisable(false);
			}
		});
						
		this.serialComm.addObserver(this);
	}
	
	
	/*util functions*/
	
	private boolean checkInputDir(String input) {
		int dir = -1;
		try {
			dir = Integer.valueOf(input);
		} catch(Exception e) { }
		return (dir >= ANGLE_MIN && dir <= ANGLE_MAX) ? true : false;
	}
	
	private boolean checkInputTime(String input) {
		int time =-1;
		try {
			time = Integer.valueOf(input);
		} catch(Exception e) { }
		return (time >= TIME_MIN && time <= TIME_MAX) ? true : false;
	}
	
	private void disableNodes(Collection<Node> btns) {
		for( Node b : btns) {
			b.setDisable(true);
		}
	}
	
	private void enableNodes(Collection<Node> btns) {
		for(Node b : btns) {
			b.setDisable(false);
		}
	}

    @Override
    public void update(Observable o, Object arg) {
        Platform.runLater( () -> {
            String msg = this.serialComm.receiveMsg();
            if (msg.contains("Welcome to Auto Mode!") || msg.contains("Welcome to Single Mode!")) {
                enableNodes(Stream.of(insertTime, pot, textAreaSA)
                        .collect(Collectors.toList()));
                disableNodes(Stream.of(inputDir, insertDir, textAreaM)
                        .collect(Collectors.toList()));
                pot.setSelected(false);
                inputTime.setDisable(false);
            } else if (msg.contains("Welcome to Manual Mode!")) {
                enableNodes(Stream.of(inputDir, insertDir, textAreaM)
                        .collect(Collectors.toList()));
                disableNodes(Stream.of(inputTime, insertTime, pot, textAreaSA)
                        .collect(Collectors.toList()));
            }
            this.textArea.appendText(msg);
            
        });
    }
}
