#include "ScanTask.h"

/**
 * @author Elena Rughi, Yuqi Sun
 * */

ScanTask::ScanTask(SharedSystem* sharedSystem, UserConsole* userConsole, ServoMotorImpl* motor, Sonar* sonar) {
  this->sharedSystem = sharedSystem;
  this->userConsole = userConsole;
  this->motor = motor;
  this->sonar = sonar;
  this->motor->on();
  this->state = SetUp;
  this->pos = 0;
  this->cycle = 0;
}

void ScanTask::init(int period) {
  Task::init(period);
}

bool ScanTask::isActive() {
  return sharedSystem->getScanMode() == ScanMode::Active;
}

void ScanTask::updatePeriod() {
  init(sharedSystem->getScanPeriod());
}

void ScanTask::tick() {
  switch(state) {
    case SetUp:
      this->updatePeriod();
      pos = sharedSystem->getPosition();
      state = Scan;
      cycle = this->wait();
      break;
    case Scan:
      motor->setPosition(pos);
      if (--cycle >= 0) {
        state = Scan;
      } else {
      /*gets distance*/
      userConsole->displayDirection();
      float dist = sonar->getDistance();
      sharedSystem->setDistance(dist);

      /*activate BlinkTask*/
      if (dist >= 0.05) {
        sharedSystem->setBlinkMode(BlinkMode::On);
      }
      /*activate AlarmTask*/
      if (dist >= DIST_MIN && dist <= DIST_MAX && sharedSystem->getSystemMode() == SystemMode::Auto){
        alarm = true;
        sharedSystem->setAlarmMode(AlarmMode::On);
      }
      userConsole->displayScanResults();
      update();
      }
      break;
  }
}

void ScanTask::reset() {
  sharedSystem->setScanMode(ScanMode::End);
  motor->setPosition(0);
  motor->off();
  state = SetUp;
  pos = 0;
  tmp = 0;
  cycle = 0;
  motor->on();
}

void ScanTask::update() {
  if (sharedSystem->getSystemMode() == SystemMode::Manual || pos >= 180) {
    sharedSystem->setScanMode(ScanMode::End);
    if (!alarm) {
      sharedSystem->setAlarmMode(AlarmMode::Off);
    }
    alarm = false;
    state = SetUp;
  } else {
    pos += 20;
    cycle = this->wait();
    sharedSystem->setPosition(pos);
    state = Scan;
  }
}

/* Calculates how many empty ticks ScanTask has to do. The estimated time from going to
 * position A to B is found by doing (pos-tmp)*7, where pos is the position where the
 * servo has to go (position B) and tmp is the current position of the servo (position A).
 * The time is then divided by scanPeriod.
 * 
 * 7 is an number found through experiments and might not be exact with other servos
 * except the one in our possession.
 */
int ScanTask::wait() {
  int time = (pos-tmp)*7;
  if (time < 0) {
    time=-time;
  }
  tmp = pos;
  return round(time/sharedSystem->getScanPeriod());
}
