#ifndef __MANUALTASK__
#define __MANUALTASK__

#include "Task.h"
#include "../Console/UserConsole.h"

/**
 * The task remains in Idle state until it receives a valid direction input from the user; then it activates
 * ScanTask. Once the scan has ended, it returns to Idle state and waits for a new direction. 
 * @author Elena Rughi, Yuqi Sun
 * */

class ManualTask: public Task {
private:
  enum { SetUp, Idle, Scan } state;
  SharedSystem* sharedSystem;
  UserConsole* userConsole;
  bool activated = false;
  int pos;

public:
  ManualTask(SharedSystem* sharedSystem, UserConsole* userConsole);
  void init(int period);
  void tick();
  bool isActive();
  void reset();
};

#endif
