#ifndef __ALARMTASK__
#define __ALARMTASK__

#include "Task.h"
#include "../Hardware/Led.h"
#include "../Utils/SharedSystem.h"

/**
 * A task that acts as the alarm mode of AutoTask. Once activated, it is deactivated by ScanTask
 * when no objects are found at distace between 0.2m and 0.4m during a scan.
 * @author Elena Rughi, Yuqi Sun
 * */

class AlarmTask: public Task {
private:
  enum { On, Off} state;
  SharedSystem* sharedSystem;
  Led* led;

public:
  AlarmTask(SharedSystem* sharedSystem, Led* led);
  void init(int period);
  void tick();
  bool isActive();
  void reset();
};

#endif