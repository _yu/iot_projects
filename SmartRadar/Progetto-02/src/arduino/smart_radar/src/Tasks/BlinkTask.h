#ifndef __BLINKTASK__
#define __BLINKTASK__

#include "Task.h"
#include "../Hardware/Led.h"
#include "../Utils/SharedSystem.h"

/**
 * A task that activates led blinking when objects are found. It automatically deactivates
 * itself after two blinks.
 * @author Elena Rughi, Yuqi Sun
 * */

class BlinkTask: public Task {
private:
  enum { On, Off} state;
  SharedSystem* sharedSystem;
  Led* led;
  int time;

public:
  BlinkTask(SharedSystem* sharedSystem, Led* led);
  void init(int period);
  void tick();
  bool isActive();
  void reset();
};

#endif
