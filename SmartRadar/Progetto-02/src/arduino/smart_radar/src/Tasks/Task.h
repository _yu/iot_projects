#ifndef __TASK__
#define __TASK__

#include "../Utils/config.h"
#include "../Utils/SharedSystem.h"

/**
 * Interface for all the tasks of this system.
 * @author Elena Rughi, Yuqi Sun
 * */

class Task {
  int myPeriod;
  int timeElapsed;

public:
  virtual void init(int period) {
    myPeriod = period;
    timeElapsed = 0;
  }

  virtual bool isActive() = 0;

  virtual void tick() = 0;

  /* Returns true if task's period has elapsed
   * which means the task should be executed*/
  bool updateAndCheckTime(int basePeriod) {
    timeElapsed += basePeriod;
    if (timeElapsed >= myPeriod) {
      timeElapsed = 0;
      return true;
    } else {
      return false;
    }
  }

  virtual void reset() = 0;
};

#endif
