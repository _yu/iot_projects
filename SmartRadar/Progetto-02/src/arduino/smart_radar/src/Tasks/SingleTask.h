#ifndef __SINGLETASK__
#define __SINGLETASK__

#include <avr/sleep.h>
#include "Task.h"
#include "../Console/UserConsole.h"
#include "../Hardware/Pir.h"
#include "../Hardware/Pot.h"
#include "../Utils/SharedSystem.h"
#include "../Utils/TimeInput/TimeInput.h"

/**
 * The task remains in Idle state until the pir detects a movement; if after 200 ticks, the pir
 * doesn't detect any movements, it calls the power saving mode. Once the pir detects a movement,
 * the system will wake up and continue from where it was stopped.
 * 
 * The user is able to choose scan time through manual input or potentiometer. SingleTask automatically
 * activates ScanTask with the last valid scan time inserted by user. When the user changes from
 * potentiometer to manual input and viceversa, scan time is reset and SingleTask remains in Time
 * state waiting for a new scan time.
 * 
 * After every scan, the pir is recalibrated.
 * @author Elena Rughi, Yuqi Sun
 * */

class SingleTask: public Task {
private:
  enum { SetUp, Idle, Time, Scan } state;
  SharedSystem* sharedSystem;
  TimeInput* timeInput;
  UserConsole* userConsole;
  Pir* pir;
  Pot* potentiometer;
  int time;
  bool movementDetected;
  void sleep();
  static void wakeUp();

public:
  SingleTask(SharedSystem* sharedSystem, UserConsole* userConsole, Pir* pir, Pot* potentiometer);
  void init(int period);
  void tick();
  bool isActive();
  void reset();
};

#endif
