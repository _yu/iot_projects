#include "ModeTask.h"

/**
 * @author Elena Rughi, Yuqi Sun
 * */

ModeTask::ModeTask(SharedSystem* sharedSystem, UserConsole* userConsole, Button* manualBt,
                   Button* autoBt, Button* singleBt, Task* blinkTask, Task* alarmTask,
                   Task* scanTask, Task* manualTask, Task* singleTask, Task* autoTask) {
  this->sharedSystem = sharedSystem;
  this->userConsole = userConsole;
  this->manualBt = manualBt;
  this->autoBt = autoBt;
  this->singleBt = singleBt;
  tasks[0] = blinkTask;
  tasks[1] = alarmTask;
  tasks[2] = scanTask;
  tasks[3] = manualTask;
  tasks[4] = singleTask;
  tasks[5] = autoTask;
}

void ModeTask::init(int period) {
  Task::init(period);
}

bool ModeTask::isActive() {
  return true;
}

void ModeTask::tick() {
  if (manualBt->isPressed()) {
    sharedSystem->setSystemMode(SystemMode::Manual);
  } else if (autoBt->isPressed()) {
    sharedSystem->setSystemMode(SystemMode::Auto);
  } else if (singleBt->isPressed()) {
    sharedSystem->setSystemMode(SystemMode::Single);
  }
  userConsole->getSystemMode();
  if (sharedSystem->getChanged()) {
    for (Task *task : tasks) { // notify all observers
      task->reset();
    }
    this->reset();
    delayMicroseconds(10000);
  }
} 

void ModeTask::reset() {
  sharedSystem->setChanged(false);
}