#ifndef __SCANTASK__
#define __SCANTASK__

#include "Task.h"
#include <math.h>
#include "../Console/UserConsole.h"
#include "../Utils/Servo/ServoMotorImpl.h"
#include "../Utils/SharedSystem.h"

/**
 * The task starts scanning the given direction (if in ManualMode) or from 0° to 180° (if in SingleMode
 * or AutoMode). It activates BlinkTask (when objects are found) and activates AlarmTask
 * (when object are found within 0.2m and 0.4m of distance) and deactivates it 
 * (when no near objects are found in an entire scan).
 * @author Elena Rughi, Yuqi Sun
 * */

class ScanTask: public Task{
private:
  enum { SetUp, Scan } state;
  SharedSystem* sharedSystem;
  UserConsole* userConsole;
  ServoMotorImpl* motor;
  Sonar* sonar;
  int pos;
  int tmp = 90;
  int cycle;
  void update();
  int wait();
  bool alarm = false;

public:
  ScanTask(SharedSystem* sharedSystem, UserConsole* userConsole, ServoMotorImpl* motor, Sonar* sonar);
  void init(int period);
  void tick();
  bool isActive();
  void updatePeriod();
  void reset();
};

#endif
