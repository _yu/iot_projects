#include "ManualTask.h"

#define MANUAL_SCAN_PERIOD 100

/**
 * @author Elena Rughi, Yuqi Sun
 * */

ManualTask::ManualTask(SharedSystem* sharedSystem, UserConsole* userConsole) {
  this->sharedSystem = sharedSystem;
  this->userConsole = userConsole;
  this->state = SetUp;
  this->pos = -1;
  this->sharedSystem->setScanPeriod(MANUAL_SCAN_PERIOD);
}

void ManualTask::init(int period) {
  Task::init(period);
}

bool ManualTask::isActive() {
  return sharedSystem->getSystemMode() == SystemMode::Manual;
}

void ManualTask::tick() {
  switch(state) {
    case SetUp:
      Serial.println("Welcome to Manual Mode!");
      Serial.println("Please enter a direction between 0 and 180");
      Serial.println("");
      state = Idle;
      break;
    case Idle:
      pos = userConsole->getServoDir();
      if (pos != -1) {
        sharedSystem->setPosition(pos);
        sharedSystem->setScanMode(ScanMode::Active);
        sharedSystem->setScanPeriod(MANUAL_SCAN_PERIOD);
        state = Scan;
      }
      break;
    case Scan:
      if (sharedSystem->getScanMode() == ScanMode::End) {
        state = Idle;
      }
      break;
    }
}

void ManualTask::reset() {
  state = SetUp;
  pos = -1;
}
