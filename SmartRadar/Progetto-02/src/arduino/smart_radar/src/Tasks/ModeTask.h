#ifndef __MODETASK__
#define __MODETASK__

#include "Task.h"
#include "../Console/UserConsole.h"
#include "../Hardware/Button.h"
#include "../Utils/SharedSystem.h"

/**
 * @author Elena Rughi, Yuqi Sun
 * */

class ModeTask: public Task {
private:
  SharedSystem* sharedSystem;
  UserConsole* userConsole;
  Button* manualBt;
  Button* autoBt;
  Button* singleBt;
  Task *tasks[6];

public:
  ModeTask(SharedSystem* sharedSystem, UserConsole* userConsole, Button* manualBt,
           Button* autoBt, Button* singleBt, Task* blinkTask, Task* alarmTask,
           Task* scanTask, Task* manualTask, Task* singleTask, Task* autoTask);
  void init(int period);
  void tick();
  bool isActive();
  void reset();
};

#endif
