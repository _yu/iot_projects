#include "BlinkTask.h"

/**
 * @author Elena Rughi, Yuqi Sun
 * */

BlinkTask::BlinkTask(SharedSystem* sharedSystem, Led* led) {
  this->sharedSystem = sharedSystem;
  this->led = led;
  this->state = Off;
  this->time = 0;
}

void BlinkTask::init(int period) {
  Task::init(period);
}

bool BlinkTask::isActive() {
  return sharedSystem->getBlinkMode() == BlinkMode::On;
}

void BlinkTask::tick() {
  switch (state) {
    case Off:
      led->switchOn();
      time++;
      state = On;
      break;
    case On:
      led->switchOff();
      state = Off;
      if (time == 2) {
        sharedSystem->setBlinkMode(BlinkMode::End);
        time = 0;
      }
      break;
  }
}

void BlinkTask::reset() {
  led->switchOff();
  sharedSystem->setBlinkMode(BlinkMode::Off);
  state = Off;
  time = 0;
}
