#ifndef __AUTOTASK__
#define __AUTOTASK__

#include "Task.h"
#include "../Utils/SharedSystem.h"
#include "../Console/UserConsole.h"
#include "../Hardware/Pot.h"
#include "../Utils/TimeInput/TimeInput.h"

/**
 * The task will stay in idle state until the 50th tick. The user is able to choose scan time through manual
 * input or potentiometer. After 50 ticks, AutoTask automatically activates ScanTask with the last valid
 * scan time inserted by user. When the user changes from potentiometer to manual input and viceversa, scan
 * time is reset and the task remains in Idle state until there is a new time.
 * When the scan is finished, the task returns to Idle state.
 * @author Elena Rughi, Yuqi Sun
 * */

class AutoTask: public Task {
private:
  enum { SetUp, Idle, Scan } state;
  SharedSystem* sharedSystem;
  TimeInput* timeInput;
  UserConsole* userConsole;
  Pot* potentiometer;
  int pos;
  int time;

public:
  AutoTask(SharedSystem* sharedSystem, UserConsole* userConsole, Pot* potentiometer);
  void init(int period);
  void tick();
  bool isActive();
  void reset();
};

#endif
