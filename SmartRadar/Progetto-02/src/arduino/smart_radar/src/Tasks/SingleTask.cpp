#include "SingleTask.h"

/**
 * @author Elena Rughi, Yuqi Sun
 * */

SingleTask::SingleTask(SharedSystem* sharedSystem, UserConsole* userConsole, Pir* pir, Pot* potentiometer) {
  this->sharedSystem = sharedSystem;
  this->userConsole = userConsole;
  this->pir = pir;
  this->potentiometer = potentiometer;
  this->timeInput = new TimeInput(sharedSystem, userConsole, potentiometer);
  this->time = 0;
  this->movementDetected = false;
  this->state = SetUp;
}

void SingleTask::init(int period) {
  Task::init(period);
}

bool SingleTask::isActive() {
  return sharedSystem->getSystemMode() == SystemMode::Single;
}

void SingleTask::tick() {
  switch(state) {
    case SetUp:
      timeInput->reset();
      Serial.println("Welcome to Single Mode!");
      Serial.println("Please enter desired time.");
      pir->calibrate();
      state = Idle;
      break;
    case Idle:
      if (!pir->movDetected()) {
        time++;
      } else {
        movementDetected = true;
      }
      if (time == 200) {
        Serial.println("zzz...");
        delay(10);
        sleep();
        movementDetected = true;
      }
      if (movementDetected) {
        Serial.println("");
        Serial.println("Movement detected!");
        Serial.println("");
        state = Time;
      }
      break;
    case Time:
      timeInput->checkTimeInput();
      if (timeInput->isInputValid()) {
        sharedSystem->setScanMode(ScanMode::Active);
        sharedSystem->setPosition(0);
        state = Scan;
      }
      break;
    case Scan:
      if (sharedSystem->getScanMode() == ScanMode::End && sharedSystem->getBlinkMode() == BlinkMode::End) {
        state = Idle;
        time = 0;
        movementDetected = false;
        pir->calibrate();
      }
      break;
  }
}

void SingleTask::reset() {
  state = SetUp;
  time = 0;
  movementDetected = false;
}

void SingleTask::sleep() {
  sleep_enable();
  attachInterrupt(0, wakeUp, RISING);
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_mode();
}

void SingleTask::wakeUp() {
  sleep_disable();//Disable sleep mode
}
