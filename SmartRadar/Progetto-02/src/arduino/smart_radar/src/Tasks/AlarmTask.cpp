#include "AlarmTask.h"

/**
 * @author Elena Rughi, Yuqi Sun
 * */

AlarmTask::AlarmTask(SharedSystem* sharedSystem, Led* led) {
  this->sharedSystem = sharedSystem;
  this->led = led;
  this->state = Off;
}

void AlarmTask::init(int period) {
  Task::init(period);
}

bool AlarmTask::isActive() {
  return sharedSystem->getAlarmMode() == AlarmMode::On;
}

void AlarmTask::tick() {
  switch (state) {
    case Off:
      led->switchOn();
      state = On;
      break;
    case On:
      led->switchOff();
      state = Off;
      break;
  }
}

void AlarmTask::reset() {
  sharedSystem->setAlarmMode(AlarmMode::Off);
  led->switchOff();
  state = Off;
}
