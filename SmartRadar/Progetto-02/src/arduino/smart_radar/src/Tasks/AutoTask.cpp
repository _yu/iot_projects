#include "AutoTask.h"

/**
 * @author Elena Rughi, Yuqi Sun
 * */

AutoTask::AutoTask(SharedSystem* sharedSystem, UserConsole* userConsole, Pot* potentiometer) {
  this->sharedSystem = sharedSystem;
  this->userConsole = userConsole;
  this->potentiometer = potentiometer;
  this->timeInput = new TimeInput(sharedSystem, userConsole, potentiometer);
  this->state = SetUp;
  this->time = 0;
}

void AutoTask::init(int period) {
  Task::init(period);
}

bool AutoTask::isActive() {
  return sharedSystem->getSystemMode() == SystemMode::Auto;
}

void AutoTask::tick() {
  switch(state) {
    case SetUp:
      timeInput->reset();
      Serial.println("Welcome to Auto Mode!");
      Serial.println("Please enter desired scan time.");
      Serial.println("");
      state = Idle;
      break;
    case Idle:
      time++;
      if (time >= 50) {
        timeInput->checkTimeInput();
        if(timeInput->isInputValid()){
          sharedSystem->setScanMode(ScanMode::Active);
          sharedSystem->setPosition(0);
          state = Scan;
        } 
      }
      break;
    case Scan:
      if (sharedSystem->getScanMode() == ScanMode::End) {
        state = Idle;
        time = 0;
        Serial.println("");
      }
      break;
  }
}

void AutoTask::reset() {
  state = SetUp;
  time = 0;
}
