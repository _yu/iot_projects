#include "UserConsole.h"

/**
 * @author Elena Rughi, Yuqi Sun
 * */

UserConsole::UserConsole(SharedSystem* sharedSystem) {
  this->sharedSystem = sharedSystem;
}

 /* ----------output to console---------- */
void UserConsole::displayDirection() {
  String s = String(sharedSystem->getPosition());
  String pos = "Scanning direction: " + s;
  Serial.println(pos);
}

void UserConsole::displayScanResults() {
  String results = "";
  float dist = sharedSystem->getDistance();
  if ( dist < 0.05) {
    results = "No object detected";
  } else {
    String s = String(dist);
    results = "Object found at distance = " + s;
  }
  Serial.println(results);
}

 /* ----------input from console---------- */

 /* Checks if there is any direction input; if there is, it resets msg. */
int UserConsole::getServoDir() {
  int pos = -1;
  if (msg.substring(0, 3) == "P: ") {
    String userInput = msg.substring(3);
    pos = userInput.toInt();
    msg = "";
    this->getMsg();
  }
  return pos;
}

/* Checks if there is any scan time input; if there is, it resets msg. */
int UserConsole::getScanTime() {
  int time = -1;
  if (msg.substring(0, 3) == "T: ") {
    String userInput = msg.substring(3);
    time = userInput.toInt();
    msg = "";
    this->getMsg();
  }
  return time;
}

/* Receives system mode changing command; it resets msg. */
void UserConsole::getSystemMode() {
  this->getMsg();
  if (msg == "M") {
    sharedSystem->setSystemMode(SystemMode::Manual);
    msg = "";
    this->getMsg();
  } else if (msg == "A"){
    sharedSystem->setSystemMode(SystemMode::Auto);
    msg = "";
    this->getMsg();
  } else if (msg == "S") {
    sharedSystem->setSystemMode(SystemMode::Single);
    msg = "";
    this->getMsg();
  }
}

/* Returns true if user has chosen to input scan time with the potentiometer.
 * It does not reset msg; this way, if there are no other user inputs, it will
 * always return true until the user changes system mode or disables the button.
 */
bool UserConsole::usePot() {
  if (msg == "Pot") {
    this->getMsg();
    return true;
  } else {
    return false;
  }
}

void UserConsole::getMsg() {
  if (MsgService.isMsgAvailable()) {
    msg = MsgService.receiveMsg();
  }
}
