#ifndef __USER_CONSOLE__
#define __USER_CONSOLE__

#include "../MsgService/MsgService.h"
#include "../Utils/SharedSystem.h"
#include "Arduino.h"

/**
 * A class that receives and sends data. It acts as link between Arduino and GUI.
 * @author Elena Rughi, Yuqi Sun
 * */

class UserConsole {
private:
  SharedSystem* sharedSystem;
  String msg;
  void getMsg();

public:
  UserConsole(SharedSystem* sharedSystem);

  /* output to console */
  void displayDirection();
  void displayScanResults();

  /* input from console */
  int getServoDir();
  int getScanTime();
  void getSystemMode();
  bool usePot();

};

#endif
