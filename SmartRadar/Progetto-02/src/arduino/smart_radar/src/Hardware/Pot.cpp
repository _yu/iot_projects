#include "Pot.h"
#include "Arduino.h"

/**
 * @author Elena Rughi, Yuqi Sun
 * */

Pot::Pot(int pin) {
  this->pin = pin;
}

int Pot::getValue() {
  return analogRead(pin);
}