#ifndef __SONAR__
#define __SONAR__

/**
 * @author Elena Rughi, Yuqi Sun
 * */

class Sonar {
private:
  int trigPin;
  int echoPin;
  
public:
  Sonar(int trigPin, int echoPin);
  float getDistance();
};

#endif
