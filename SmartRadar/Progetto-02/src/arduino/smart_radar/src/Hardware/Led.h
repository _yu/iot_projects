#ifndef __LED__
#define __LED__

/**
 * @author Elena Rughi, Yuqi Sun
 * */

class Led {
private:
  int pin;  

public:
  Led(int pin);
  void switchOn();
  void switchOff();
};

#endif
