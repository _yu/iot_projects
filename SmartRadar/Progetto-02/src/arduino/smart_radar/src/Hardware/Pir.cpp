#include "Pir.h"
#include "Arduino.h"

/**
 * @author Elena Rughi, Yuqi Sun
 * */

Pir::Pir(int pin) {
  this->pin = pin;
  pinMode(pin, INPUT);
}

void Pir::calibrate() { 
  Serial.println("");
  Serial.println("Calibrating sensor...");
  for (int i = 0; i < CALIBRATION_TIME_SEC; i++) {
    Serial.println("...");
    delay(1000);
  }
  Serial.println("Pir sensor ready!");
  delay(50);
}

bool Pir::movDetected() {
  return digitalRead(pin) == HIGH;
}