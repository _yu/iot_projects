#include "Sonar.h"
#include "Arduino.h"

/**
 * @author Elena Rughi, Yuqi Sun
 * */

#define TEMPERATURE 20
#define NO_OBJ 38

Sonar::Sonar(int trigPin, int echoPin) {
  this->trigPin = trigPin;
  this->echoPin = echoPin;
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);  
}

float Sonar::getDistance() {
  const double vs = 331.45 + 0.62*TEMPERATURE;
  /* send impulse */
  digitalWrite(trigPin,LOW);
  delayMicroseconds(3);
  digitalWrite(trigPin,HIGH);
  delayMicroseconds(5);
  digitalWrite(trigPin,LOW);

  /* receive echo */
  long tUS = pulseInLong(echoPin, HIGH);
  
  /*
   * HC-SR04 datasheet specifies that, if no obstacle is detected,
   * the output pin will give a 38ms high level signal 
   */
  if (tUS == NO_OBJ) {
    return -1;
  } else {
    double t = tUS / 1000.0 / 1000.0 / 2;
    double d = t*vs;
    return d;
  }
}
