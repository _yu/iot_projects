#ifndef __PIR__
#define __PIR__

#define CALIBRATION_TIME_SEC 3

/**
 * @author Elena Rughi, Yuqi Sun
 * */

class Pir {
private:
    int pin;

public:
    Pir(int pin);
    bool movDetected();
    void calibrate();
};

#endif
