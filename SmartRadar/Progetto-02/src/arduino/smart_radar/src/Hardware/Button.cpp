#include "Button.h"
#include "Arduino.h"

/**
 * @author Elena Rughi, Yuqi Sun
 * */

Button::Button(int pin) {
  this->pin = pin;
  pinMode(pin, INPUT);
}

bool Button::isPressed() {
  return digitalRead(pin) == HIGH;
}

int Button::getPin() {
  return pin;
}