#ifndef __POT__
#define __POT__

/**
 * @author Elena Rughi, Yuqi Sun
 * */

class Pot {
private:
  int pin;

public:
  Pot(int pin);
  int getValue();
};

#endif
