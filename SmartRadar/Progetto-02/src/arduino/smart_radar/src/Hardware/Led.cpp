#include "Led.h"
#include "Arduino.h"

/**
 * @author Elena Rughi, Yuqi Sun
 * */

Led::Led(int pin) {
  this->pin = pin;
  pinMode(pin,OUTPUT);
}

void Led::switchOn() {
  digitalWrite(pin,HIGH);
}

void Led::switchOff() {
  digitalWrite(pin,LOW);
}