#ifndef __BUTTON__
#define __BUTTON__

/**
 * @author Elena Rughi, Yuqi Sun
 * */

class Button {
private:
  int pin;

public:
  Button(int pin);
  bool isPressed();
  int getPin();
};

#endif
