#ifndef __SCHEDULER__
#define __SCHEDULER__

#include "../Utils/Timer/Timer.h"
#include "../Tasks/Task.h"

#define MAX_TASKS 10

/**
 * @author Elena Rughi, Yuqi Sun
 * */

class Scheduler {
private:
  int basePeriod;
  int nTasks;
  Task* taskList[MAX_TASKS];
  Timer timer;

public:
  void init(int basePeriod);
  virtual bool addTask(Task* task);
  virtual void schedule();
};

#endif
