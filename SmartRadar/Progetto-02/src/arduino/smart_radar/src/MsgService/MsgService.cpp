#include "Arduino.h"
#include "MsgService.h"

/**
 * @author Elena Rughi, Yuqi Sun
 * */

String content;

MsgServiceClass MsgService;

bool MsgServiceClass::isMsgAvailable() {
  return msgAvailable;
}

String MsgServiceClass::receiveMsg() {
  String msg = currentMsg;
  msgAvailable = false;
  currentMsg = "";
  content = "";
  return msg;  
}

void MsgServiceClass::init() {
  Serial.begin(9600);
  content.reserve(256);
  content = "";
  currentMsg = "";
  msgAvailable = false;  
}

void serialEvent() {
  /* reading the content */
  while (Serial.available()) {
    char ch = (char) Serial.read();
    if (ch == '\n') {
      MsgService.currentMsg = content;
      MsgService.msgAvailable = true;      
    } else {
      content += ch;      
    }
  }
}



