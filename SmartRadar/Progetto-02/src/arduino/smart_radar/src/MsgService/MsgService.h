#ifndef __MSGSERVICE__
#define __MSGSERVICE__

#include "Arduino.h"

/**
 * @author Elena Rughi, Yuqi Sun
 * */

class MsgServiceClass {
    
public: 
  String currentMsg;
  bool msgAvailable;
  void init();  
  bool isMsgAvailable();
  String receiveMsg();
};

extern MsgServiceClass MsgService;

#endif

