
/**
 * @author Elena Rughi, Yuqi Sun
 * */

#ifndef __CONFIG__
#define __CONFIG__

/* environment settings */
#define SECTIONS 10

/* time(seconds) */
#define TIME_MAX 10000
#define TIME_MIN 2000
#define TIME_DEFAULT 5000

/* distance(cm) */
#define DIST_MIN 0.2
#define DIST_MAX 0.4

#define PIN_LED_A 13
#define PIN_LED_D 12
#define PIN_PIR 2
#define PIN_POT A5
#define PIN_ECHO 4
#define PIN_TRIG 5
#define PIN_SERVO 6
#define PIN_BTN_MANUAL 8
#define PIN_BTN_SINGLE 9
#define PIN_BTN_AUTO 10

#endif
