#ifndef __SCAN_TIME_UTILS__
#define __SCAN_TIME_UTILS__

#include <math.h>
#include "../config.h"
#include "../../Console/UserConsole.h"
#include "../SharedSystem.h"
#include "../../Hardware/Pot.h"

/**
 * A class that receives time inputs from the user and checks if they are valid.
 * If an input is valid, the given scan period is set in sharedSystem
 * @author Elena Rughi, Yuqi Sun
 * */

class TimeInput {
private:
  SharedSystem* sharedSystem;
  UserConsole* userConsole;
  Pot* pot;
  bool once;
  bool valid = false;
  int dividend;
  int time = 0;
  void setTime(int t);
  void resetManualInput();
  void resetPotentiometer();

public:
  TimeInput(SharedSystem* sharedSystem, UserConsole* userConsole, Pot* pot);
  void checkTimeInput();
  bool isInputValid();
  void reset();
};

#endif
