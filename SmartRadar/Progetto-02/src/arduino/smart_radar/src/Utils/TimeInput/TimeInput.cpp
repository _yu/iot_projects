#include "TimeInput.h"

/**
 * @author Elena Rughi, Yuqi Sun
 * */

TimeInput::TimeInput(SharedSystem* sharedSystem, UserConsole* userConsole, Pot* pot) {
  this->sharedSystem = sharedSystem;
  this->userConsole = userConsole;
  this->pot = pot;
  this->dividend = floor(1023000/(TIME_MAX - TIME_MIN));
  this->once = false;
}

void TimeInput::checkTimeInput() {
  if (userConsole->usePot()) {
    this->resetPotentiometer();
    int pot_input = pot->getValue();
    int pot_time = (floor(pot_input/dividend))*1000 + TIME_MIN;
    setTime(pot_time);
  } else {
    this->resetManualInput();
    int console_input = userConsole->getScanTime();
    if (console_input >= 0){
      setTime(console_input);
    }
  } 
}

bool TimeInput::isInputValid() {
  return valid;
}

void TimeInput::setTime(int t) {
  if (time != t) {
    time = t;
    valid = sharedSystem->setScanPeriod(t / (SECTIONS*2));
    if (valid) {
      String s = "Scan time set to: " + String(time);
      Serial.println(s);
    } else {
     Serial.println("Scan time not valid");
    }
  }
}

void TimeInput::reset() {
  valid = false;
  time = 0;
}

void TimeInput::resetPotentiometer() {
  if (!once) {
    this->reset();
    once = true;
  }
}

void TimeInput::resetManualInput() {
  if (once) {
    this->reset();
    once = false;
  }
}



