#ifndef __SHAREDSYSTEM__
#define __SHAREDSYSTEM__

#include "../Hardware/Pot.h"
#include "../Hardware/Sonar.h"
#include "config.h"
#include "Arduino.h"

enum class SystemMode { Auto, Single, Manual };
enum class ScanMode { Active, Inactive, End };
enum class BlinkMode { On, Off, End };
enum class AlarmMode { On, Off };

/**
 * A class that includes all the variables shared among the tasks
 * and the main state of the whole system.
 * @author Elena Rughi, Yuqi Sun
 * */

class SharedSystem {

private:
  SystemMode systemMode;
  ScanMode scanMode;
  BlinkMode blinkMode;
  AlarmMode alarmMode;
  bool changed;
  int pos;
  float dist;
  int scanPeriod;

public:
  SharedSystem(){
    systemMode = SystemMode::Manual;
    scanMode = ScanMode::Inactive;
    blinkMode = BlinkMode::Off;
    alarmMode = AlarmMode::Off;
    changed = false;
    pos = 0;
    dist = 0;
    scanPeriod = TIME_DEFAULT;
  }

  bool getChanged() {
    return changed;
  }

  void setChanged(bool reset) {
    changed = reset;
  }

  void setSystemMode(SystemMode newMode) {
    if (this->systemMode != newMode) {
      this->systemMode = newMode;
      Serial.println("");
      Serial.println("Changing mode...");
      Serial.println("");
      changed = true;
    }
  }

  SystemMode getSystemMode(){
    return systemMode;
  }

  void setScanMode(ScanMode newMode) {
    this->scanMode = newMode;
  }

  ScanMode getScanMode(){
    return scanMode;
  }

  void setBlinkMode(BlinkMode newMode) {
    this->blinkMode = newMode;
  }

  BlinkMode getBlinkMode(){
    return blinkMode;
  }

  void setAlarmMode(AlarmMode newMode) {
    this->alarmMode = newMode;
  }

  AlarmMode getAlarmMode(){
    return alarmMode;
  }


  void setPosition(int pos) {
    this->pos = pos;
  }

  int getPosition(){
    return pos;
  }

  void setDistance(float distance) {
    this->dist = distance;
  }

  float getDistance() {
    return dist;
  }

  bool setScanPeriod(int newPeriod) {
    if(newPeriod >= TIME_MIN/(SECTIONS*2) && newPeriod <= TIME_MAX/(SECTIONS*2)){
      this->scanPeriod = newPeriod;
      return true;
    } else {
      return false;
    }
  }

  int getScanPeriod() {
    if (systemMode == SystemMode::Manual) {
      return scanPeriod;
    } else {
      return scanPeriod*2;
    }
  }
  
};

#endif
