#ifndef __SERVO_MOTOR_IMPL__
#define __SERVO_MOTOR_IMPL__

#include <arduino.h>
#include <math.h>
#include "ServoMotor.h"
#include "ServoTimer2.h"

/**
 * @author Elena Rughi, Yuqi Sun
 * */

class ServoMotorImpl: public ServoMotor {

public:
  ServoMotorImpl(int pin);

  void on();
  void setPosition(int angle);
  void off();

private:
  int pin;
  ServoTimer2 motor;
};

#endif
