#ifndef __TIMER__
#define __TIMER__

/**
 * @author Alessandro Ricci
 * */

class Timer {
    
public:  
  Timer();
  /* period in ms */
  void setupPeriod(int period);  
  void waitForNextTick();

};


#endif

