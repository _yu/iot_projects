#include <Arduino.h>

#include "src/Tasks/Task.h"
#include "src/Scheduler/Scheduler.h"
#include "src/Utils/SharedSystem.h"
#include "src/Utils/Servo/ServoMotorImpl.h"
#include "src/MsgService/MsgService.h"

#include "src/Tasks/ManualTask.h"
#include "src/Tasks/SingleTask.h"
#include "src/Tasks/AutoTask.h"

#include "src/Tasks/ModeTask.h"
#include "src/Tasks/ScanTask.h"
#include "src/Tasks/BlinkTask.h"
#include "src/Tasks/AlarmTask.h"

#define PERIOD_SCHED 50
#define PERIOD_MODE 50
#define PERIOD_BLINK 50
#define PERIOD_ALARM 50
#define PERIOD_SCAN_DEFAULT 100
#define PERIOD_MANUAL 100
#define PERIOD_SINGLE 100
#define PERIOD_AUTO 100

/**
 * @author Elena Rughi, Yuqi Sun
 * */

Scheduler scheduler;

void setup() {
  MsgService.init();
  scheduler.init(PERIOD_SCHED);

  SharedSystem* system = new SharedSystem();
  UserConsole* userConsole = new UserConsole(system);
  Pir* pir = new Pir(PIN_PIR);
  Pot* potentiometer = new Pot(PIN_POT);
  ServoMotorImpl* servo = new ServoMotorImpl(PIN_SERVO);
  Sonar* sonar = new Sonar(PIN_TRIG, PIN_ECHO);
  Led* ledA = new Led(PIN_LED_A);
  Led* ledD = new Led(PIN_LED_D);
  Button* manualBt = new Button(PIN_BTN_MANUAL);
  Button* singleBt = new Button(PIN_BTN_SINGLE);
  Button* autoBt = new Button(PIN_BTN_AUTO);
  
  Task* t1 = new BlinkTask(system, ledD);
  t1->init(PERIOD_BLINK);

  Task* t2 = new AlarmTask(system, ledA);
  t2->init(PERIOD_ALARM);
  
  Task* t3 = new ScanTask(system, userConsole, servo, sonar);
  t3->init(PERIOD_SCAN_DEFAULT);
   
  Task* t4 = new ManualTask(system, userConsole);
  t4->init(PERIOD_MANUAL);

  Task* t5 = new SingleTask(system, userConsole, pir, potentiometer);
  t5->init(PERIOD_SINGLE);
  scheduler.addTask(t4);

  Task* t6 = new AutoTask(system, userConsole, potentiometer);
  t6->init(PERIOD_AUTO);

  Task* t0 = new ModeTask(system, userConsole, manualBt, autoBt, singleBt, t1, t2, t3, t4, t5, t6);
  t0->init(PERIOD_MODE);
  
  scheduler.addTask(t0);
  scheduler.addTask(t1);
  scheduler.addTask(t2);
  scheduler.addTask(t3);
  scheduler.addTask(t4);
  scheduler.addTask(t5);
  scheduler.addTask(t6);
  
}

void loop() {
  scheduler.schedule();
}
