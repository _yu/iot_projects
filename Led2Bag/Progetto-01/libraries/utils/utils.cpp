#include <Arduino.h>
#include "utils.h"

void clearFlagPin2() {
    EIFR = (1 << INTF0);
}

void clearFlagPin3() {
    EIFR = (1 << INTF1);
}

int getLevel(int analogLevel) {
    return analogLevel/143;
}

/*reduce DT time of its 1/8*/
void updateTimer(int *period, int numObj) {
    if (numObj != 0) {
        *period = *period - *period/8;
    }
}

void updateLedPos(int *pos, int *delta) {
    if (*pos == 3 || *pos == 1) {
        *delta = -*delta;
    }
    *pos += *delta;
}
