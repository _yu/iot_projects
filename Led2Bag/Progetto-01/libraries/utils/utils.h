#ifndef HEADER_H
#define HEADER_H

void clearFlagPin2();

void clearFlagPin3();

int getLevel(int analogLevel);

void updateLedPos(int *pos, int *delta);

void updateTimer(int *period, int numObj);

#endif
