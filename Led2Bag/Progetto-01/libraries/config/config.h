#ifndef _CONFIG_
#define _CONFIG_

/* pins */
#define GREEN_3 13
#define GREEN_2 12
#define GREEN_1 10
#define WHITE 6
#define RED 8
#define TS 3
#define TD 2
#define LEVEL A5    //potentiometer

/* game states */
#define INTRO 0
#define SETUP 1
#define SETLED 2
#define PLAYING 3
#define GAMEOVER 4

#endif
