#ifndef _LEDUTILS_
#define _LEDUTILS_

void blinkLed(int pin);

void turnLedsOff();

bool ledFade(int pin);

void resetFade();

void randLedOn(int *pos, int leds[4]);

#endif
