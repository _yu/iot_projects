#include <Arduino.h>
#include "led_utils.h"
#include "config.h"

#define LED_LOOP_TIME 100

static int fadeAmount = 1;
int intensity = 0;

void blinkLed(int pin) {
    digitalWrite(pin, HIGH);
    delay(LED_LOOP_TIME);
    digitalWrite(pin, LOW);
}

void turnLedsOff() {
    digitalWrite(GREEN_1, LOW);
    digitalWrite(GREEN_2, LOW);
    digitalWrite(GREEN_3, LOW);
    digitalWrite(WHITE, LOW);
}

/*when called repeatedly the given led executes a fade
  returns true if the led has completed a full fade*/
bool ledFade(int pin) {
    analogWrite(pin, intensity);
    intensity = intensity + fadeAmount;
    if (intensity == 0 || intensity == 255){
        fadeAmount = -fadeAmount;
    }
    delayMicroseconds(3500);
    return intensity == 0;
}

void resetFade(){
    intensity = 0;
}

void randLedOn(int *pos, int leds[4]){
    noInterrupts();
    *pos = rand()%3+1;
    digitalWrite(leds[*pos], HIGH);
    interrupts();
}
