#include "utils.h"
#include "config.h" /*pin association*/
#include "led_utils.h"

int leds[4] = {WHITE, GREEN_1, GREEN_2, GREEN_3};
static int levels[8] = {3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000};
int numObj = 0;
volatile byte gameState = INTRO;

/*time related variables*/
volatile int period = 0;
volatile long timeLastDT = millis(); /*solves TD button's bouncing*/
volatile long timeGame = 0;
static int bouncingDelay = 100;

/*utility variables*/
volatile int pos = 3;
volatile bool enabledTD = false;
int delta = 1;
bool fading = false;
bool setupIntro = false;

void setup() {
    pinMode(GREEN_1, OUTPUT);
    pinMode(GREEN_2, OUTPUT);
    pinMode(GREEN_3, OUTPUT);
    pinMode(WHITE, OUTPUT);
    resetFade();
    Serial.begin(9600);
}

void loop() {
    noInterrupts();
    int state = gameState;
    interrupts();

    switch(state){
        case INTRO: intro(); break;
        case SETUP: setupGame(); break;
        case SETLED: setLed();
                     timeGame = millis(); break; //start game timer
        case PLAYING: gameLoop(); break;
        case GAMEOVER: gameOver(); break;
        default: ;
    }
}


/*--------------------GAME FUNCTIONS-----------------------*/


void intro() {
    /*enable TS button*/
    if (!setupIntro) {
        clearFlagPin3(); /*resets interrupt flag for TS pin*/
        attachInterrupt(digitalPinToInterrupt(TS), pressTS, RISING);
        Serial.println("Welcome to Led to Bag. Press Key TS to Start");
        setupIntro = true;
        pos = 3;
        delta = 1;
    }
    /*loop of the green leds*/
    blinkLed(leds[pos]);
    /*ISR of TD is not attached yet, therefore there's no need to protect pos;
      for this reason, noInterrupts()/interrupts() were not added*/
    updateLedPos(&pos, &delta);
}

void setupGame() {
    /*enable TD button*/
    clearFlagPin2();
    attachInterrupt(digitalPinToInterrupt(TD), pressTD, FALLING);
    /*set timer period based on potentiometer*/
    int difficulty = getLevel(analogRead(LEVEL));
    period = levels[difficulty];
    /*start game*/
    Serial.println("Go!");
    turnLedsOff();
    setGameState(SETLED);
}

/*turns on a random green led; starts the game*/
void setLed() {
    updateTimer(&period, numObj);
    String string1 = "Time: ";
    String string2 = string1 + period;
    Serial.println(string2);
    randLedOn(&pos, &leds[0]);
    setGameState(PLAYING);
}

void gameLoop() {
    /*check timer*/
    if (!fading && millis() - timeGame >= period) {
        setGameState(GAMEOVER);
    }
    /*check in which position the object is
      0 = white led,
      -1 = red led,
      1,2,3 = green leds */
    if (gameState==PLAYING && enabledTD) {
        noInterrupts();
        int objPos = pos;
        interrupts();
        if (objPos == 0) {
            fading = true;
            /*ledFade returns true when led completes a fade-in-out*/
            bool endFade = ledFade(WHITE);
            if (endFade) {
                numObj++;
                win();
            }
        } else if (objPos < 0) {
            setGameState(GAMEOVER);
        } else if (objPos > 0) {
            /*turn on next green led*/
            digitalWrite(leds[objPos], HIGH);
            enabledTD = false;
        }
    }
}

void gameOver() {
    detachInterrupt(digitalPinToInterrupt(TD));
    turnLedsOff();
    /*print score*/
    String string1 = "Game Over - Score: ";
    String string2 = string1 + numObj;
    Serial.println(string2);
    /*red led on for 2 secs*/
    digitalWrite(RED, HIGH);
    delay(2000);
    digitalWrite(RED, LOW);
    resetToIntro();
}

void resetToIntro() {
    setupIntro = false;
    enabledTD = false;
    fading = false;
    numObj = 0;
    resetFade();
    setGameState(INTRO);
}

void win() {
    String string1 = "Another object in the bag! Count: ";
    String string2 = string1 + numObj;
    String string3 = string2 + " objects";
    Serial.println(string3);
    /*reset game parameters*/
    turnLedsOff();
    resetFade();
    enabledTD = false;
    fading = false;
    /*start next game*/
    setGameState(SETLED);
}

void setGameState(int state) {
    noInterrupts();
    gameState = state;
    interrupts();
}

/*ISR - Start Button*/
void pressTS() {
    gameState = SETUP;
    detachInterrupt(digitalPinToInterrupt(TS));
}

/*ISR - Down Button*/
void pressTD() {
    unsigned long timeTD = millis();
    if (timeTD - timeLastDT > bouncingDelay && (gameState==PLAYING)) {
        digitalWrite(leds[pos], LOW);
        pos--;
        enabledTD = true;
        timeLastDT = millis();
    }
}
