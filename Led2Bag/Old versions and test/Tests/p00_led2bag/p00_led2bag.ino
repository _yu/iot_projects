#include <functions.h>
//leds
#define GREEN_1 12
#define GREEN_2 11
#define GREEN_3 10
#define WHITE 9   //PWD
#define RED 8
//buttons; these pins support interrupt
#define BUTTON_S 2
#define BUTTON_D 3

void setup() {
  //leds -> output
  for(int pin = 8; pin <= 12; pin++){
    pinMode(pin, OUTPUT);
  }
  //buttons -> input
  pinMode(BUTTON_S, INPUT);
  pinMode(BUTTON_D, INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:

}
