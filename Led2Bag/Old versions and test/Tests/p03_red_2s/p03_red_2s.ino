#define RED 9

int redState = LOW;
unsigned long prevMillis = 0;
const long redInterval = 2000;

void setup() {
  pinMode(RED, OUTPUT);
}

void loop() {
  //code that u need to be running all the time

  //led time check
  unsigned long currMillis = millis();
  if(currMillis - prevMillis >= redInterval){
    //save last time you blinked the LED
    prevMillis = currMillis;
    switchStates();
    digitalWrite(RED, redState);
  }
}

void switchStates(){
   if(redState == LOW){
    redState = HIGH;
   } else {
    redState = LOW;
   }
}
