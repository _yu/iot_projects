#define GREEN_1 12
#define GREEN_2 11
#define GREEN_3 10

int timer = 100;

void setup() {
  for(int pin = 10; pin <= 12; pin++){
    pinMode(pin, OUTPUT);
  }
}

void loop() {
  //loop from the lowest pin to the highest
  for(int thisPin = 10; thisPin <= 12; thisPin++){
    digitalWrite(thisPin, HIGH);
    delay(timer);
    digitalWrite(thisPin, LOW);
  }

  //loop from the highest pin to the lowest
  for(int thisPin = 12; thisPin >= 10; thisPin--){
    digitalWrite(thisPin, HIGH);
    delay(timer);
    digitalWrite(thisPin, LOW);
  }
}
