#ifndef HEADER_H
#define HEADER_H

#include <Arduino.h>

//void ledsLoop(int pin1, int pin2, int pin3);
void clearFlagPin2();
void clearFlagPin3();
int getLevel(int analogLevel);
void updateLedPos(int *pos, int *delta);
void turnLedOnAndOff(int pin);
void turnAllLedsOff(int pin1, int pin2, int pin3);
void updateTimer(int *period, int numObj);
void resetFade();
void changeGameState(bool *state1, bool *state2);
bool ledFade(int pin);

#endif
