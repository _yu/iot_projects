#include <Arduino.h>
#include "functions.h"

#define LED_LOOP_TIME 100

static int fadeAmount = 1;
int intensity = 0;

void clearFlagPin2() {
    EIFR = (1 << INTF0);
}

void clearFlagPin3() {
    EIFR = (1 << INTF1);
}

int getLevel(int analogLevel) {
    return analogLevel/143;
}

void updateLedPos(int *pos, int *delta) {
    if (*pos == 3 || *pos == 1) {
      *delta = -*delta;
    }
    *pos += *delta;
}

// used for green leds loop -used in pregame()
void turnLedOnAndOff(int pin) {
  digitalWrite(pin, HIGH);
  delay(LED_LOOP_TIME);
  digitalWrite(pin, LOW);
}

// used to turn off the given 3 leds
// maybe we should rename it
/*given a pointer to array of leds, do a for loop????*/
void turnAllLedsOff(int pin1, int pin2, int pin3) {
    digitalWrite(pin1, LOW);
    digitalWrite(pin2, LOW);
    digitalWrite(pin3, LOW);
}

/*reduce DT time of its 1/8*/
void updateTimer(int *period, int numObj) {
    if (numObj != 0) {
        *period = *period - *period/8;
    }
}

// when called repeatedly the given led executes a fade
bool ledFade(int pin) {
  analogWrite(pin, intensity);
  intensity = intensity + fadeAmount;
  if (intensity == 0 || intensity == 255){
    fadeAmount = -fadeAmount;
  }
  delayMicroseconds(3500);
  return intensity == 0;
}

void resetFade(){
    intensity = 0;
}

void changeGameState(bool *state1, bool *state2) {
    *state1 = false;
    *state2 = true;
}
