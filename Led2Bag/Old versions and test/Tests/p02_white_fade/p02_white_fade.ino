#define WHITE 9

int brightness;
int fade;
int intensity;
unsigned long prevMillis = 0;
const long interval = 2350;

void setup() {
  intensity = 0;
  fade = 1;
  pinMode(WHITE, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  //led time check
  unsigned long currMillis = millis();

  if(currMillis<= interval){
    analogWrite(WHITE, intensity);
    intensity = intensity + fade;
    Serial.println(intensity);
    if (intensity == 0 || intensity == 255){
      fade = -fade;
    }
  } else{
    digitalWrite(WHITE,LOW);
  }
}
