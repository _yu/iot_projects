#include "functions.h"

#define GREEN_3 13
#define GREEN_2 12
#define GREEN_1 10
#define WHITE 6
#define RED 8
#define TS 3
#define TD 2
#define LEVEL A5
#define LED_LOOP_TIME 100

/*game's states*/
volatile bool stateIntro = true;
volatile bool stateSetGame = false;
volatile bool stateSetLed = false;
volatile bool statePlaying = false;
volatile bool stateGameOver = false;

int leds[4] = {WHITE, GREEN_1, GREEN_2, GREEN_3};
static int levels[8] = {3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000};
int numObj = 0;

/*utility variables*/
volatile int pos = 3;
volatile int period = 0;
volatile bool enabledTD = false;
/*TODO once decided how to implement a better timer decide the type of period*/
volatile long timeLastDT = millis(); /*to be use to solve TD button's bouncing*/
volatile long timeGame = 0; /*for timer*/
static int bouncingDelay = 100;
int delta = 1;
bool fading = false;
bool setUpIntro = false;

void setup() {
    pinMode(GREEN_1, OUTPUT);
    pinMode(GREEN_2, OUTPUT);
    pinMode(GREEN_3, OUTPUT);
    pinMode(WHITE, OUTPUT);
    resetFade();
    Serial.begin(9600);
}

void loop() {
    //noInterrupts();
    if (stateIntro) {
        preGame();
    } else if (stateSetGame) {
        setGame();
    } else if (stateSetLed){
        setLed();
        timeGame = millis();
    } else if(statePlaying) {
      gameLoop();
      Serial.println(fading);
    } else if(stateGameOver){
      gameOver();
      resetToPreGame();
    }
    //interrupts();
}

void preGame() {
    if (!setUpIntro) {
        clearFlagPin3();
        attachInterrupt(digitalPinToInterrupt(TS), pressTS, RISING);
        Serial.println("Welcome to Led to Bag. Press Key TS to Start");
        setUpIntro = true;
    }

    /*loop of the green leds*/
    turnLedOnAndOff(leds[pos]);
    /*ISR of TD is not enabled yet, therefore pos is only modified here;
    for this reason, noInterrupts()/interrupts() was not added*/
    updateLedPos(&pos, &delta);
}

void setGame() {
    clearFlagPin2();
    attachInterrupt(digitalPinToInterrupt(TD), pressTD, FALLING);
    int difficulty = getLevel(analogRead(LEVEL));
    period = levels[difficulty];
    Serial.println("Go!");
    turnAllLedsOff(GREEN_1, GREEN_2, GREEN_3);
    noInterrupts();
    changeGameState(&stateSetGame, &stateSetLed);
    interrupts();
}

void setLed() {
    updateTimer(&period, numObj);
    Serial.println(period);
    randLed();
    changeGameState(&stateSetLed, &statePlaying);
}

void gameLoop() {
    /*checks timer*/
    if (millis() - timeGame >= period && !fading) {
        setGameOver();
    }

    if (enabledTD & statePlaying) {
        noInterrupts();
        int tmp = pos;
        interrupts();
        if (tmp == 0) {
            fading = true;
            bool fade = ledFade(WHITE);
            if (fade) {
                numObj++;
                String string1 = "Another object in the bag! Count: ";
                String string2 = string1 + numObj;
                String string3 = string2 + " objects";
                Serial.println(string3);
                reset();
            }
        } else if (tmp < 0) {
            setGameOver();
        } else if (tmp > 0) {
            digitalWrite(leds[pos], HIGH);
            //noInterrupts();
            enabledTD = false;
            //interrupts();
        }
    }
}

void gameOver() {
  detachInterrupt(digitalPinToInterrupt(TD));
  digitalWrite(WHITE, LOW);
  turnAllLedsOff(GREEN_1, GREEN_2, GREEN_3);
  String string1 = "Game Over - Score: ";
  String string2 = string1 + numObj;
  Serial.println(string2);
  digitalWrite(RED, HIGH);
  delay(2000);
  digitalWrite(RED, LOW);
}

/*TRANSFORM INTO FUNCTION*/
void randLed(){
  noInterrupts();
  pos = rand()%3+1;
  digitalWrite(leds[pos], HIGH);
  interrupts();
}


void setGameOver(){
    changeGameState(&statePlaying, &stateGameOver);
}

void reset() {
  turnAllLedsOff(GREEN_1, GREEN_2, GREEN_3);
  digitalWrite(WHITE, LOW);
  resetFade();
  fading = false;
  //noInterrupts();
  enabledTD = false;
  //interrupts();
  statePlaying = false;
  stateSetLed = true;
}

void resetToPreGame() {
    resetFade();
    setUpIntro = false;
    //noInterrupts();
    enabledTD = false;
    //interrupts();
    numObj = 0;
    delta = 1;
    noInterrupts();
    pos = 3;
    changeGameState(&stateGameOver, &stateIntro);
    interrupts();
}

// ISR - Start Button
void pressTS() {
    changeGameState(&stateIntro, &stateSetGame);
    detachInterrupt(digitalPinToInterrupt(TS));
}

//ISR - Down Button
void pressTD(){
    unsigned long timeTD = millis();
    if (timeTD - timeLastDT > bouncingDelay && statePlaying) {
        digitalWrite(leds[pos], LOW);
        pos--;
        enabledTD = true;
        timeLastDT = millis();
    }
}
