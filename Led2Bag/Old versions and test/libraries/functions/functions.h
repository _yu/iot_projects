#ifndef HEADER_H
#define HEADER_H

#include <Arduino.h>

//void ledsLoop(int pin1, int pin2, int pin3);
void turnLedOnAndOff(int pin);
void turnAllLedsOff(int pin1, int pin2, int pin3);
void checkDifficulty();
void updateTimer(unsigned long *period);
void resetFade();
boolean ledFade(int pin);

#endif
