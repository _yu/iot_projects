#include <Arduino.h>
#include "functions.h"

#define LED_LOOP_TIME 100

static int fadeAmount = 1;
static int intensity = 0;

/*
void ledsLoop(int pin1, int pin2, int pin3) {
    turnLedOnAndOff(pin3);
    turnLedOnAndOff(pin2);
    turnLedOnAndOff(pin1);
    turnLedOnAndOff(pin2);
}*/

// used for green leds loop -used in pregame()
void turnLedOnAndOff(int pin) {
  digitalWrite(pin, HIGH);
  delay(LED_LOOP_TIME);
  digitalWrite(pin, LOW);
}

// used to turn off the given 3 leds
// maybe we should rename it
void turnAllLedsOff(int pin1, int pin2, int pin3) {
    digitalWrite(pin1, LOW);
    digitalWrite(pin2, LOW);
    digitalWrite(pin3, LOW);
}

/*reduce DT time of its 1/8*/
void updateTimer(unsigned long *period) {
  *period = *period - *period/8;
}

// when called repeatedly the given led executes a fade
boolean ledFade(int pin) {
  analogWrite(pin, intensity);
  intensity = intensity + fadeAmount;
  if (intensity == 0 || intensity == 255){
    fadeAmount = -fadeAmount;
  }
  delayMicroseconds(3500);
  return intensity == 0;
}

void resetFade(){
    intensity = 0;
}
