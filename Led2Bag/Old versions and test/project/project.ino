#include "TimerOne.h"
#include "functions.h"

#define GREEN_1 10
#define GREEN_2 12
#define GREEN_3 13
#define WHITE 6
#define RED 8
#define TS 3
#define TD 2
#define LEVEL A5
#define LED_LOOP_TIME 100
/*
#define PREGAME 1
#define START 2
#define SETLED 3
#define GAME 4
#define GAMEOVER 5
*/
volatile bool stateIntro = true;
volatile bool stateStart = false;
volatile bool stateSetLed = false;
volatile bool stateGame = false;
volatile bool stateGameOver = false;
/*volatile int gameStatus = PREGAME;*/
volatile bool fading = false;
bool stopTimer = true;
bool printWelcome = true;
volatile bool enabledTD = false;
volatile bool enabledTS = true;

volatile int pos = 3;
int delta = 1;
int bouncingDelay = 100;
unsigned long startTime = millis();
int numObj = 0;
int leds[4] = {WHITE, GREEN_1, GREEN_2, GREEN_3};
int levels[8] = {3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000};
unsigned long startDT = 0;

unsigned long period;

void setup() {
  pinMode(GREEN_1, OUTPUT);
  pinMode(GREEN_2, OUTPUT);
  pinMode(GREEN_3, OUTPUT);
  pinMode(WHITE, OUTPUT);
  resetFade();
  Serial.begin(9600);
  attachInterrupt(digitalPinToInterrupt(TS), pressTS, RISING);
  attachInterrupt(digitalPinToInterrupt(TD), pressTD, FALLING);
  period = 5000ul;
}

void loop() {
  if (stateIntro) {
    preGame();
  } else if (stateStart) {
    Serial.println(period);
    startGame();
    stateStart = false;
    stateSetLed = true;
  } else if (stateSetLed){
    if (numObj != 0) {
      updateTimer(&period);
    }
    Serial.println(period);
    stateSetLed = false;
    stateGame = true;
    randLed();
    startDT = millis();
  } else if(stateGame) {
    gameLoop();
  } else if(stateGameOver){
    gameOver();
    /*function that RESET EVERYTHING!!!!*/
    printWelcome = true;    
    enabledTS = true;
    enabledTD = false;
    numObj = 0;
    stateGameOver = false;
    stateIntro = true;
    period = 5000;
    pos = 3;
    delta = 1;
  }
}

void preGame() {
  if (printWelcome) {
      Serial.println("Welcome to Led to Bag. Press Key TS to Start");
      printWelcome = false;
  }

  // loop of the green leds
  turnLedOnAndOff(leds[pos]);
  if (pos == 3 || pos == 1) {
    delta = -delta;
  }
  pos += delta;

  /*FIND WAY TO AVOID LEVEL POLLING*/
  // check intial game difficulty (time interavl)
  int level = checkLevel();
  period = levels[level];
}

void startGame() {
  Serial.println("Go!");
  turnAllLedsOff(GREEN_1, GREEN_2, GREEN_3);
}

void gameLoop() { 
  /*CAN TIMER POLLING BE AVOIDED????
  PROF COULD HAVE TIMER CODE
  OR MAYBE ARDUINO.CC*/
  if (millis()-startDT >= period && !fading) {
    setGameOver();
  }
  if (enabledTD) {
      if (pos == 0) {
      fading = true;
      bool fade = ledFade(WHITE);
        if (fade) {
          numObj++;
          String string1 = "Another object in the bag! Count: ";
          String string2 = string1 + numObj;
          String string3 = string2 + " objects";
          Serial.println(string3);
          reset();
        }
      } else if (pos < 0) {
        setGameOver();
      } else {
        digitalWrite(leds[pos], HIGH);
        enabledTD = false;
      }
  }
}

void gameOver() {
  digitalWrite(WHITE, LOW);
  turnAllLedsOff(GREEN_1, GREEN_2, GREEN_3);
  String string1 = "Game Over - Score: ";
  String string2 = string1 + numObj;
  Serial.println(string2);
  digitalWrite(RED, HIGH);
  delay(2000);
  digitalWrite(RED, LOW);
}

/*TRANSFORM INTO FUNCTION*/
void randLed(){
  pos = rand()%3+1;
  digitalWrite(leds[pos], HIGH);
}


void setGameOver(){
  stateGame = false;
  stateGameOver = true;
}

void reset() {
  turnAllLedsOff(GREEN_1, GREEN_2, GREEN_3);
  digitalWrite(WHITE, LOW);
  resetFade();
  fading = false;
  enabledTD = false;
  stateGame = false;
  stateSetLed = true;
}

// check Game Level => can this be transformed into function?
int checkLevel() {
  int level = analogRead(LEVEL);
  Serial.println(level);
  return level/143;
}

// ISR - Start Button
void pressTS() {
  if(enabledTS){
    stateIntro = false;
    stateStart = true;
    enabledTS = false;
  }
}

//ISR - Down Button
void pressTD(){
  unsigned long tdTime = millis();
  if(tdTime - startTime > bouncingDelay && stateGame) {
    digitalWrite(leds[pos], LOW);
    pos--;
    enabledTD = true;
    startTime = millis();
  }
}

